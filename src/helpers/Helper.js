export default class Helper {
  constructor() { }

  /**
   * Получить HEADER для запроса
   * @returns
   */
  static GetHeader() {
    let token = localStorage.getItem("token");
    return {
      Accept: "application/json",
      Authorization: "Bearer " + token,
    };
  }

  /**
   * Получить OPTIONS для axios 
   * @returns 
   */
  static GetOptions() {
    return {
      headers: Helper.GetHeader(),
    }
  }
}
