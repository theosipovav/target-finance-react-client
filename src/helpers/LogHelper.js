import Helper from "./Helper";
import axios from "axios";
import { GET_LOGS, DELETE_LOG } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о сделках
 */
class LogHelper extends Helper {

    /**
   * Получить все записи о импорте для текущего пользователя
   * @returns 
   */
    static async GetAll() {
        return axios.get(GET_LOGS, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
     *  Удалить запись об импорте
     * @param {*} id Идентификатор
     * @returns 
     */
    static async Remove(id) {
        return axios.delete(DELETE_LOG + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }



}
export default LogHelper;