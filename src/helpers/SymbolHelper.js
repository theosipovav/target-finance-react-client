import Helper from "./Helper";
import axios from "axios";
import { GET_SYMBOL_ALL } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о символах
 */
class SymbolHelper extends Helper {

    /**
     * Получить все виды символы
     * @returns 
     */
    static async GetAll() {
        return axios.get(GET_SYMBOL_ALL, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

}
export default SymbolHelper;