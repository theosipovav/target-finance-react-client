import Helper from "./Helper";
import axios from "axios";
import { GET_ROLE_ALL } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о сделках
 */
class RoleHelper extends Helper {

    /**
   * Получить все записи о импорте для текущего пользователя
   * @returns 
   */
    static async GetAll() {
        return axios.get(GET_ROLE_ALL, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

}
export default RoleHelper;