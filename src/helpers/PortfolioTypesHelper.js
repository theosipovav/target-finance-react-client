import Helper from "./Helper";
import axios from "axios";
import { GET_PORTFOLIO_TYPE } from "../rest/urls";

class PortfolioTypesHelper extends Helper {

  /**
   * Получить типы портфеля по его идентификатору
   * @param {*} id Идентификатор портфеля
   * @returns
   */
  static GetAll() {
    const headers = Helper.GetHeader();
    return axios
      .get(GET_PORTFOLIO_TYPE, { headers: headers })
      .then((response) => {
        return { success: true, data: response.data };
      })
      .catch((error) => {
        console.log(error.response);
        return { success: false, data: error.response.data };
      });
  }
}

export default PortfolioTypesHelper;
