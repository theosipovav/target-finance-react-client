import Helper from "./Helper";
import axios from "axios";
import { URL_SIGNUP, GET_USER_ALL, DELETE_USER, GET_USER_PASSWORD_RESET, GET_USER_FOR_LOGIN, GET_USERS } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о сделках
 */
class UsersHelper extends Helper {



    /**
       * Получить все записи о импорте для текущего пользователя
       * @returns 
       */
    static async Signup(data) {

        return axios
            .post(URL_SIGNUP, data)
            .then((res) => {
                return { success: true, data: res.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });

    }


    /**
       * Получить все записи о импорте для текущего пользователя
       * @returns 
       */
    static async OneForLogin(login) {
        return axios.get(GET_USER_FOR_LOGIN + "/" + login, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
   * Получить все записи о импорте для текущего пользователя
   * @returns 
   */
    static async GetAll() {
        return axios.get(GET_USER_ALL, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
   * Получить все записи о импорте для текущего пользователя
   * @returns 
   */
    static async All() {
        return axios.get(GET_USERS, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
     *  Удалить запись об импорте
     * @param {*} id Идентификатор
     * @returns 
     */
    static async Remove(id) {
        return axios.delete(DELETE_USER + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }

    /**
     *  Удалить запись об импорте
     * @param {*} id Идентификатор
     * @returns 
     */
    static async ResetPassword(id) {
        return axios.get(GET_USER_PASSWORD_RESET + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }


}
export default UsersHelper;