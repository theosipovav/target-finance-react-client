import Helper from "./Helper";
import axios from "axios";
import { GET_ASSET_CATEGORY_ALL, POST_ASSET_CATEGORY_CREATE, PUT_ASSET_CATEGORY_UPDATE, DELETE_ASSET_CATEGORY_DELETE } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о сделках
 */
class AssetCategoryHelper extends Helper {

    /**
     * Получить все классы актива
     * @returns 
     */
    static async GetAll() {
        return axios.get(GET_ASSET_CATEGORY_ALL, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }


    /**
    * Создать новую запись в базе данных
    * @returns 
    */
    static async Create(formData) {
        return axios.post(POST_ASSET_CATEGORY_CREATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }
    /**
     * Обновить запись в базе данных
     * @returns 
     */
    static async Update(formData) {
        return axios.put(PUT_ASSET_CATEGORY_UPDATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
     * Удалить запись из базы данных
     * @returns 
     */
    static async Remove(id) {
        return axios.delete(DELETE_ASSET_CATEGORY_DELETE + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }

}
export default AssetCategoryHelper;