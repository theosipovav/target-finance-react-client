import Helper from "./Helper";
import axios from "axios";
import { GET_TRADES, GET_TRADE_BY_ID, GET_TRADE_UPDATE, REMOVE_TRADE_UPDATE, POST_TRADE_CREATE, GET_GROUP_BY_MONTH_FOR_PRICE } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о сделках
 */
class TradeHelper extends Helper {



    /**
     * Получить все сделки текущего пользователя
     * @returns 
     */
    static async GetAll() {
        return axios.get(GET_TRADES, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }


    /**
     * Получить сделку по её идентификатору
     * @returns 
     */
    static async GetOneById(id) {
        return axios.get(GET_TRADE_BY_ID + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }


    /**
     * Обновить данные сделки
     * @param {*} data
     * @returns
     */
    static async Update(data) {
        const formData = new FormData();
        Object.keys(data).forEach((key) => {
            formData.append(key, data[key]);
        });
        return axios
            .post(GET_TRADE_UPDATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }

    /**
    * Создать
    * @param {*} data
    * @returns
    */
    static async Create(data) {
        const formData = new FormData();
        Object.keys(data).forEach((key) => {
            formData.append(key, data[key]);
        });
        return axios
            .post(POST_TRADE_CREATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
     * Удалить сделку по ее идентификатору
     * @param {*} id Идентификатор
     * @returns 
     */
    static async Remove(id) {
        return axios.delete(REMOVE_TRADE_UPDATE + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }


    /**
     * Получить отчет стоимсоти всех активов по месяцам в портфели
     * @returns 
     */
    static async GetGroupByMonthForPrice(id) {
        return axios.get(GET_GROUP_BY_MONTH_FOR_PRICE + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

}
export default TradeHelper;