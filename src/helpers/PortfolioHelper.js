import Helper from "./Helper";
import axios from "axios";
import { GET_PORTFOLIOS_USER, GET_PORTFOLIO_ID, POST_PORTFOLIO_UPDATE, GET_SUMMARY_FROM_ASSET_CATEGORY, GET_SUMMARY_FROM_ASSET_SYMBOLS } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о портфелях
 */
class PortfolioHelper extends Helper {



  /**
 * Получить все портфели пользователя
 * @returns 
 */
  static async GetAll() {
    return axios.get(GET_PORTFOLIOS_USER, Helper.GetOptions())
      .then((response) => {
        return { success: true, data: response.data };
      })
      .catch((error) => {
        console.log(error.response);
        return { success: false, data: error.response.data };
      });
  }

  /**
   * Получить портфель по его идентификатору
   * @param {*} id Идентификатор портфеля
   * @returns
   */
  static GetPortfolioById(id) {
    return axios
      .get(GET_PORTFOLIO_ID + "/" + id, { headers: Helper.GetHeader() })
      .then((response) => {
        if (response.status == 200) {
          return response.data;
        } else {
          console.log(response);
          return false;
        }
      })
      .catch((error) => {
        console.log(error);
        return false;
      });
  }

  /**
 * Получить портфель по его идентификатору
 * @param {*} id Идентификатор портфеля
 * @returns
 */
  static GetOne(id) {
    return axios
      .get(GET_PORTFOLIO_ID + "/" + id, { headers: Helper.GetHeader() })
      .then((response) => {
        return { success: true, data: response.data };
      })
      .catch((error) => {
        console.log(error.response);
        return { success: false, data: error.response.data };
      });
  }

  /**
   * Получить портфель по его идентификатору
   * @param {*} id Идентификатор портфеля
   * @returns
   */
  static GetSummaryFromAssetCategory(id) {
    return axios
      .get(GET_SUMMARY_FROM_ASSET_CATEGORY + "/" + id, { headers: Helper.GetHeader() })
      .then((response) => {
        if (response.status == 200) {
          return { success: true, data: response.data };
        } else {
          return { success: false, data: response.data };
        }
      })
      .catch((error) => {
        return { success: false, data: error.response.data };
      });
  }
  /**
   * Получить портфель по его идентификатору
   * @param {*} id Идентификатор портфеля
   * @returns
   */
  static GetSummaryFromSymbols(id) {
    return axios
      .get(GET_SUMMARY_FROM_ASSET_SYMBOLS + "/" + id, { headers: Helper.GetHeader() })
      .then((response) => {
        if (response.status == 200) {
          return { success: true, data: response.data };
        } else {
          return { success: false, data: response.data };
        }
      })
      .catch((error) => {
        return { success: false, data: error.response.data };
      });
  }

  /**
   * Обновить портфель
   * @param {*} data
   * @returns
   */
  static UpdatePortfolio(formData) {
    return axios
      .post(POST_PORTFOLIO_UPDATE, formData, Helper.GetOptions())
      .then((response) => {
        return { success: true, data: response.data };
      })
      .catch((error) => {
        console.log(error.response);
        return { success: false, data: error.response.data };
      });
  }



}

export default PortfolioHelper;
