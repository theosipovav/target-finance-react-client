import Helper from "./Helper";
import axios from "axios";
import { GET_STATUS_ALL, POST_STATUS_CREATE, PUT_STATUS_UPDATE, DELETE_STATUS_DELETE } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о сделках
 */
class StatusHelper extends Helper {

    /**
     * Получить все записи из базы данных
     * @returns 
     */
    static async GetAll() {
        return axios.get(GET_STATUS_ALL, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }


    /**
     * Создать новую запись в базе данных
     * @returns 
     */
    static async Create(formData) {
        return axios.post(POST_STATUS_CREATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }
    /**
     * Обновить запись в базе данных
     * @returns 
     */
    static async Update(formData) {
        return axios.put(PUT_STATUS_UPDATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
     * Удалить запись из базы данных
     * @returns 
     */
    static async Remove(id) {
        return axios.delete(DELETE_STATUS_DELETE + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }

}
export default StatusHelper;