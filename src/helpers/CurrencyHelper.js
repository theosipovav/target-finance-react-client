import Helper from "./Helper";
import axios from "axios";
import { GET_CURRENCY_ALL, POST_CURRENCY_CREATE, GET_CURRENCY_REMOVE, GET_CURRENCY_UPDATE } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о валютах
 */
class CurrencyHelper extends Helper {

    /**
     * Получить все записи из базы данных
     * @returns 
     */
    static async GetAll() {
        return axios.get(GET_CURRENCY_ALL, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }


    /**
     * Создать новую запись в базе данных
     * @returns 
     */
    static async Create(formData) {
        return axios.post(POST_CURRENCY_CREATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }
    /**
     * Обновить запись в базе данных
     * @returns 
     */
    static async Update(formData) {
        return axios.put(GET_CURRENCY_UPDATE, formData, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error.response);
                return { success: false, data: error.response.data };
            });
    }

    /**
     * Удалить запись из базы данных
     * @returns 
     */
    static async Remove(id) {
        return axios.delete(GET_CURRENCY_REMOVE + "/" + id, Helper.GetOptions())
            .then((response) => {
                return { success: true, data: response.data };
            })
            .catch((error) => {
                console.log(error);
                return { success: false, data: error.response.data };
            });
    }

}
export default CurrencyHelper;