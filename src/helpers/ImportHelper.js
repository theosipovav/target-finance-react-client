import Helper from "./Helper";
import axios from "axios";
import { URL_IMPORT } from "../rest/urls";

/**
 * Хелпер для получение и отправки данных о сделках
 */
class ImportHelper extends Helper {

    /**
     * Получить все статусы
     * @returns 
     */
    static async load(formData) {
        return axios.post(URL_IMPORT, formData, Helper.GetOptions()).then((response) => {
            return { success: true, data: response.data };
        }).catch((error) => {
            console.log(error.response);
            return { success: false, data: error.response.data };
        });
    }

}
export default ImportHelper;