import React from "react";
import styles from "./Footer.module.scss";
import { connect } from "react-redux";
import { Col, Row } from "react-bootstrap";
import imgFacebook from "../../img/facebook.png";
import imgInstagram from "../../img/instagram.png";
import imgTwitter from "../../img/twitter.png";
import imgRss from "../../img/rss.png";
import Navbar from "../../components/Navigation/Navbar/Navbar";

/**
 * Подвал сайта
 * @param {*} props
 * @returns
 */
const Footer = (props) => {
  let isAuthenticated = props.isAuthenticated;
  let clsFooter = [styles.Footer];
  var clsNavbarFooter = ["navbar", styles.NavbarFooter];
  if (isAuthenticated) {
    clsFooter.push("text-primary");
    clsNavbarFooter.push("navbar-light");
  } else {
    clsFooter.push("text-white-50");
    clsNavbarFooter.push("navbar-dark");
  }

  var clsFooterLink = ["btn", "btn-link", styles.FooterIconLink];

  return (
    <footer className={clsFooter.join(" ")}>
      <Row>
        <Col>
          <div className="d-flex flex-column h-100">
            <p className="flex-grow-1">
              <b>Target Finance</b> Учет и контроль ваших инвестиций <b>(c) {props.year}</b>
            </p>
            <div className="d-flex">
              <a className={clsFooterLink.join(" ")} href="#" target={"_blank"}>
                <img src={imgFacebook} height={36} />
              </a>
              <a className={clsFooterLink.join(" ")} href="#" target={"_blank"}>
                <img src={imgInstagram} height={36} />
              </a>
              <a className={clsFooterLink.join(" ")} href="#" target={"_blank"}>
                <img src={imgTwitter} height={36} />
              </a>
              <a className={clsFooterLink.join(" ")} href="#" target={"_blank"}>
                <img src={imgRss} height={36} />
              </a>
            </div>
          </div>
        </Col>
        <Col className="d-flex justify-content-end align-items-end">
          <nav className={clsNavbarFooter.join(" ")}>
            <p>
              Сервис использует файлы "cookie", с целью улучшения качества продукта.
              <br />
              «Cookie» это небольшие файлы, содержащие информацию о предыдущих посещениях веб-сайта.
              <br />
              Если вы не хотите использовать файлы "cookie", измените настройки браузера.
            </p>
            <Navbar />
          </nav>
        </Col>
      </Row>
      <Row>
        <Col className="text-center pt-1 pb-1">2022 год</Col>
      </Row>
    </footer>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.auth.user,
});

export default connect(mapStateToProps, null)(Footer);
