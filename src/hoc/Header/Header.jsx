import React from "react";
import Navbar from "../../components/Navigation/Navbar/Navbar";
import NavbarUserCard from "../../components/Navigation/NavbarUserCard/NavbarUserCard";
import styles from "./Header.modules.scss";
/**
 * Шапка сайта
 * @param {*} props
 * @returns
 */
const Header = (props) => (
  <header>
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary bg-gradient">
      <div className="container">
        <a className="navbar-brand" href="#">
          TF
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <Navbar />
          <NavbarUserCard />
        </div>
      </div>
    </nav>
  </header>
);

export default Header;
