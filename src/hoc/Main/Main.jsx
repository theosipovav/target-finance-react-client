import React from "react";
import Container from "../Container/Container";
import styles from "./Main.module.scss";
import { connect } from "react-redux";

class Main extends React.Component {
  render() {
    const cls = this.props.isAuthenticated ? styles.Main : styles.MainAuthenticated;
    return (
      <main className={cls}>
        <Container {...this.props} />
      </main>
    );
  }
}
function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.user,
  };
}

export default connect(mapStateToProps, null)(Main);
