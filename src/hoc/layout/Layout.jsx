import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "./Layout.module.scss";

const Layout = (props) => {
  let isAuthenticated = props.isAuthenticated;
  const classWrapper = [];
  const stylesContainer = ["container", "d-flex", "flex-column", "h-100"];
  if (isAuthenticated) {
    classWrapper.push(styles.wrapper);
  } else {
    classWrapper.push(styles.wrapperCover);
    classWrapper.push("bg-primary");
  }
  return (
    <div className={classWrapper.join(" ")}>
      <div className={stylesContainer.join(" ")}>{props.children}</div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.auth.user,
});

export default connect(mapStateToProps, null)(Layout);
