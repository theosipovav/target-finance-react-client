import React, { Component } from "react";
import styles from "./UserControl.module.scss";
import { Dropdown, Table } from "react-bootstrap";
import { connect } from "react-redux";
import Loader from "../../components/UI/Loader/Loader";
import UsersHelper from "../../helpers/UsersHelper";
import { MessageSet } from "../../store/actions/MessageAction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSortDown, faSortUp, faSort, faEye, faTrash, faEdit, faAlignJustify, faSearch } from "@fortawesome/free-solid-svg-icons";

/**
 * Страница "Управление пользователями"
 */
class UserControl extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);

    this.state = {
      loadingStep: 0,
      loadingMax: 1,
      users: [],
    };
  }

  /**
   * Выполняется до отрисовки компонента
   */
  componentWillMount() {
    //
  }

  handleRemove = (id) => {
    this.setState({
      loadingStep: 0,
    });
    UsersHelper.Remove(id)
      .then((res) => {
        if (res.success) {
          //
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      })
      .then(() => {
        this.load();
      });
  };

  handleResetPassword = (id) => {
    this.setState({
      loadingStep: 0,
    });
    UsersHelper.ResetPassword(id)
      .then((res) => {
        if (res.success) {
          //
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      })
      .then(() => {
        this.load();
      });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <h1 className="h3">Управление пользователями</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Логин</th>
                  <th>Электронная почта</th>
                  <th>Роль</th>
                  <th>Имя</th>
                  <th>Дата регистрации</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.state.users.map((obj, index) => {
                  var dt = new Date(obj.dtRegistration);
                  var dtString = dt.toLocaleString("ru-RU");
                  console.log();
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{obj.login}</td>
                      <td>{obj.email}</td>
                      <td>{obj.role.name}</td>
                      <td>{obj.name}</td>
                      <td>{dtString}</td>
                      <td className="text-center">
                        <Dropdown className="flex-grow-1">
                          <Dropdown.Toggle variant="link" id="dropdown-basic" className={styles.DropdownToggle}>
                            <FontAwesomeIcon icon={faAlignJustify} />
                          </Dropdown.Toggle>
                          <Dropdown.Menu>
                            <Dropdown.Item variant="link" onClick={(e) => this.handleResetPassword(obj.userId)}>
                              <FontAwesomeIcon icon={faEdit} /> Сбросить пароль
                            </Dropdown.Item>
                            <Dropdown.Item variant="link" onClick={(e) => this.handleRemove(obj.userId)} className="text-danger">
                              <FontAwesomeIcon icon={faTrash} /> Удалить
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        </div>
      </React.Fragment>
    );
  }

  load() {
    this.setState({
      loadingStep: 0,
    });

    UsersHelper.GetAll()
      .then((res) => {
        if (res.success) {
          var step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            users: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      });
  }
  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load();
  }
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(null, mapDispatchToProps)(UserControl);
