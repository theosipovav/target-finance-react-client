import React, { Component } from "react";
import { connect } from "react-redux";
import { logout } from "../../store/actions/ActionLogout";
import { Navigate } from "react-router-dom";

/**
 * Страница выхода
 * Редирект на главную
 */
export class Logout extends Component {
  componentDidMount() {
    this.props.logoutAction();
  }
  render() {
    let isAuthenticated = this.props.isAuthenticated;
    if (isAuthenticated) {
      return <Navigate to="/" />;
    } else {
    }
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logoutAction: () => dispatch(logout()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
