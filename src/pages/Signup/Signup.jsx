import React, { Component } from "react";
import styles from "./Signup.module.scss";
import UsersHelper from "../../helpers/UsersHelper";

import SignupForm from "../../components/Forms/SignupForm/SignupForm";
import { connect } from "react-redux";
import { MessageSet } from "../../store/actions/MessageAction";
import Loader from "../../components/UI/Loader/Loader";
import { Navigate } from "react-router-dom";

/**
 * Страница "Регистрация нового пользователя"
 */
class Signup extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isFirstUser: false,
      redirect: false,
    };
  }
  handleSubmit(e) {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    let form = e.target;
    let formData = new FormData(form);
    let data = {
      Login: formData.get("Login"),
      Password: formData.get("Password"),
      PasswordRepeat: formData.get("PasswordRepeat"),
      Email: formData.get("Email"),
      Name: formData.get("Name"),
    };
    UsersHelper.Signup(data)
      .then((res) => {
        this.setState({
          loading: false,
          redirect: "/signin",
        });
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response, null);
      });
  }

  render() {
    if (!!this.state.redirect) return <Navigate to={this.state.redirect} />;
    if (this.state.loading) return <Loader white="true" />;
    return (
      <div className={styles.signup}>
        <div className="row">
          <div className="col-12 text-center">
            <h1 className="h1 text-white mt-1 mb-3">Учет и контроль ваших инвестиций</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <SignupForm isFirstUser={this.state.isFirstUser} handleSubmit={(e) => this.handleSubmit(e)} />
          </div>
        </div>
      </div>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    UsersHelper.All()
      .then((res) => {
        if (res.success) {
          this.setState({
            loading: false,
            isFirstUser: res.data.length == 0 ? true : false,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response, null);
      });
  }
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(null, mapDispatchToProps)(Signup);
