import React, { Component } from "react";
import styles from "./TradeEdit.module.scss";
import { connect } from "react-redux";
import TradeForm from "../../components/Forms/TradeForm/TradeForm";
import Loader from "../../components/UI/Loader/Loader";
import AssetCategoryHelper from "../../helpers/AssetCategoryHelper";
import PortfolioHelper from "../../helpers/PortfolioHelper";
import CurrencyHelper from "../../helpers/CurrencyHelper";
import StatusHelper from "../../helpers/StatusHelper";
import SymbolHelper from "../../helpers/SymbolHelper";
import TradeHelper from "../../helpers/TradeHelper";
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Link, Navigate } from "react-router-dom";
import { MessageSet } from "../../store/actions/MessageAction";
import { createBrowserHistory } from "history";

/**
 * Страница "Редактирование сделки"
 */
class TradeEdit extends Component {
  tradeId = 0;
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    const history = createBrowserHistory();
    const arr = history.location.pathname.split("/");
    this.tradeId = parseInt(arr[3]);
    this.state = {
      loadingStep: 0,
      loadingMax: 6,
      idForm: `${"Id"}${Math.random()}`,
      portfolios: [],
      assetCategories: [],
      currencies: [],
      statuses: [],
      symbols: [],
      trade: {},
      redirect: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      loadingStep: 0,
    });
    var form = e.target;
    const formData = new FormData(form);
    const data = {
      TradeId: formData.get("TradeId"),
      PortfolioId: formData.get("PortfolioId"),
      AssetCategoryId: formData.get("AssetCategoryId"),
      CurrencyId: formData.get("CurrencyId"),
      SymbolId: formData.get("SymbolId"),
      Dt: formData.get("Dt"),
      Count: formData.get("Count"),
      PriceTransaction: formData.get("PriceTransaction"),
      PriceClose: formData.get("PriceClose"),
      Proceed: formData.get("Proceed"),
      Commission: formData.get("Commission"),
      Basis: formData.get("Basis"),
      RealizedPL: formData.get("RealizedPL"),
      MtmPL: formData.get("MtmPL"),
      StatusId: formData.get("StatusId"),
    };
    TradeHelper.Update(data)
      .then((res) => {
        if (res.success) {
          var trade = res.data;
          console.log(trade);
          this.setState({
            redirect: "/trades",
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        this.props.MessageSet(null, error.message, null);
      });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (!!this.state.redirect) return <Navigate to={this.state.redirect} />;
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;

    var dt = new Date(this.state.trade.dt);
    var dtString = dt.toLocaleDateString("ru-Ru");

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <h1 className="h3 my-3">
              Редактирование сделки #{this.state.trade.tradeId} от {dtString}
            </h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <TradeForm
              id={this.state.idForm}
              trade={this.state.trade}
              portfolios={this.state.portfolios}
              assetCategories={this.state.assetCategories}
              currencies={this.state.currencies}
              statuses={this.state.statuses}
              symbols={this.state.symbols}
              handleSubmit={(e) => this.handleSubmit(e)}
            />
            <div className="d-flex justify-content-center align-items-center">
              <Link to={"/trades"} className="btn btn-warning btn-lg mx-1">
                Отмена
              </Link>
              <Button type="submit" size="lg" form={this.state.idForm} className=" mx-1">
                Сохранить
              </Button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Загрузка данных о сделки
   * @param {*} id Идентификатор сделки
   */
  load() {
    this.setState({
      loadingStep: 0,
    });
    TradeHelper.GetOneById(this.tradeId)
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            trade: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        this.props.MessageSet(null, error.message, null);
      });

    PortfolioHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            portfolios: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    AssetCategoryHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            assetCategories: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    CurrencyHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            currencies: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    StatusHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            statuses: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    SymbolHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            symbols: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load();
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    //
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TradeEdit);
