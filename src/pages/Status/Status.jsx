import React, { Component } from "react";
import styles from "./Status.module.scss";
import { connect } from "react-redux";
import StatusHelper from "../../helpers/StatusHelper";
import { MessageSet } from "../../store/actions/MessageAction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faObjectGroup, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Button, Col, Row, Table } from "react-bootstrap";
import Loader from "../../components/UI/Loader/Loader";
import StatusForm from "../../components/Forms/StatusForm/StatusForm";

class Status extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      loadingStep: 0,
      loadingMax: 1,
      statuses: [],
    };
  }

  /**
   * Загрузка данных
   */
  load() {
    StatusHelper.GetAll()
      .then((res) => {
        if (res.success) {
          var step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            statuses: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      });
  }

  /**
   * Создать запись
   * @param {*} e
   */
  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ loadingStep: 0 });
    let form = e.target;
    let formData = new FormData(form);
    StatusHelper.Create(formData)
      .then((res) => {
        if (res.success) {
          this.load();
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response, null);
      });
  };

  /**
   * Удалить запись
   * @param {*} id
   */
  handleRemove = (id) => {
    this.setState({
      loadingStep: 0,
    });
    StatusHelper.Remove(id)
      .then((res) => {
        console.log(res);
        if (res.success) {
          this.load();
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response, null);
      });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;
    return (
      <React.Fragment>
        <Row>
          <Col>
            <h1 className="h3 mt-3">Управление статусами сделок</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <StatusForm handleSubmit={(e) => this.handleSubmit(e)} />
          </Col>
        </Row>
        <hr />
        <Row>
          <Col>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Наименование</th>
                  <th>Описание</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.state.statuses.map((obj, index) => {
                  console.log(obj);
                  return (
                    <tr key={index}>
                      <td>{index}</td>
                      <td>{obj.name}</td>
                      <td>{obj.desc}</td>
                      <td className="text-center">
                        <Button variant={"link"} className="text-danger" onClick={() => this.handleRemove(obj.statusId)}>
                          <FontAwesomeIcon icon={faTrash}></FontAwesomeIcon>
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load();
  }
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(null, mapDispatchToProps)(Status);
