import React, { Component } from "react";
import PortfolioCreateForm from "../../components/Forms/PortfolioCreateForm/PortfolioCreateForm";
import styles from "./PortfolioCreate.module.scss";

/**
 * Страница "Создание нового портфеля"
 */
class PortfolioCreate extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <h1 className="h1">Создание нового портфеля</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <PortfolioCreateForm />
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}
export default PortfolioCreate;
