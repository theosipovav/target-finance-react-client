import React, { Component } from "react";
import styles from "./Trades.module.scss";
import { connect } from "react-redux";
import Loader from "../../components/UI/Loader/Loader";
import { GET_PORTFOLIOS_USER, GET_TRADES_PORTFOLIO } from "../../rest/urls";
import { Button, Col, Modal, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faSync } from "@fortawesome/free-solid-svg-icons";
import TradeTable from "../../components/TradeTable/TradeTable";
import TradeHelper from "../../helpers/TradeHelper";
import { Link, Navigate } from "react-router-dom";
import { MessageSet } from "../../store/actions/MessageAction";

/**
 * Страница "Сделки"
 */
class Trades extends Component {
  state = {
    loading: true,
    portfolios: [],
    trades: [],
    tradeRemove: null,
    tradeSelect: null,
    redirect: false,
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Ожидание удаление
   * @param {*} id
   */
  handleRemove = (id) => {
    var trades = this.state.trades;
    var tradeRemove = trades.find((trade) => trade.tradeId === id);
    console.log(tradeRemove);
    this.setState({
      tradeRemove: tradeRemove,
    });
  };

  handleModalClose = () => {
    this.setState({
      tradeRemove: null,
    });
  };

  handleModalDelete = () => {
    this.remove();
    this.handleModalClose();
  };

  handleModalDetail = (id) => {
    TradeHelper.GetOneById(id)
      .then((res) => {
        if (res.success) {
          this.setState({
            tradeSelect: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        this.props.MessageSet(null, error.message, null);
      });
  };

  handleModalDetailClose = () => {
    this.setState({
      tradeSelect: null,
    });
  };

  handleModalEdit = (id) => {
    this.setState({
      redirect: `/trade/edit/${id}`,
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (!!this.state.redirect) return <Navigate to={this.state.redirect} />;
    if (this.state.loading) return <Loader />;
    var trades = this.state.trades;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 d-flex py-3">
            <div className="flex-grow-1">
              <h1 className="h3">Сделки</h1>
            </div>
            <div className="">
              <Link to={"/trade/create"} className="btn btn-link">
                <FontAwesomeIcon size="sm" icon={faPlus} />
              </Link>
              <Button variant="link" className="ms-1" onClick={(e) => this.load()}>
                <FontAwesomeIcon size="sm" icon={faSync} />
              </Button>
            </div>
          </div>
          <div className="col-12">
            <TradeTable trades={trades} handleRemove={this.handleRemove} handleModalDetail={(id) => this.handleModalDetail(id)} handleModalEdit={(id) => this.handleModalEdit(id)} />
          </div>
        </div>
        <this.renderModalDetail />
        <this.renderModal />
      </React.Fragment>
    );
  }

  /**
   * Модальное окно подтверждение удаления
   * @returns
   */
  renderModal = () => {
    let id = null;
    let dt = null;
    let isShow = false;
    if (this.state.tradeRemove !== null) {
      id = this.state.tradeRemove.tradeId;
      dt = this.state.tradeRemove.dt;
      isShow = true;
    }
    return (
      <React.Fragment>
        <Modal show={isShow}>
          <Modal.Header closeButton>
            <Modal.Title>Удаление сделки #{id}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Вы действительно хотите удалить сделку #{id} от {dt}?
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleModalClose}>
              Отмена
            </Button>
            <Button variant="danger" onClick={this.handleModalDelete}>
              Удалить
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  };

  /**
   * Модальное окно просмотра акции
   */
  renderModalDetail = () => {
    var isShow = !!this.state.tradeSelect;
    var trade = this.state.tradeSelect;
    var title = isShow ? `Сделка №${trade.tradeId} от ${trade.dt}` : "";
    var portfolioName = isShow ? trade.portfolio.name : "";
    var assetCategoryName = isShow ? trade.assetCategory.name : "";
    var currencyName = isShow ? trade.currency.symbol : "";
    var symbolValue = isShow ? trade.symbol.value : "";
    var statusName = isShow ? trade.status.name : "";
    var count = isShow ? trade.count : "";
    var basis = isShow ? trade.basis : "";
    var commission = isShow ? trade.commission : "";
    var priceTransaction = isShow ? trade.priceTransaction : "";
    var priceClose = isShow ? trade.priceClose : "";
    var proceed = isShow ? trade.proceed : "";
    var realizedPL = isShow ? trade.realizedPL : "";
    var mtmPL = isShow ? trade.mtmPL : "";
    return (
      <Modal size="lg" show={isShow} onHide={(e) => this.handleModalDetailClose(e)} aria-labelledby="example-modal-sizes-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg">{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body className={styles.ModalBody}>
          <div className="px-5">
            <Row>
              <Col>
                <p>
                  Портфель: <b>{portfolioName}</b>
                </p>
              </Col>
              <Col>
                <p>
                  Статус: <b>{statusName}</b>
                </p>
              </Col>
            </Row>
            <Row>
              <Col>
                <p className="lh-sm">
                  Актив: <b>{assetCategoryName}</b>
                  <br />
                  Валюта: <b>{currencyName}</b>
                  <br />
                  Тикер: <b>{symbolValue}</b>
                  <br />
                  Количество: <b>{count}</b>
                  <br />
                </p>
              </Col>
              <Col>
                <p className="lh-sm">
                  Базис: <b>{basis}</b>
                  <br />
                  Комиссия: <b>{commission}</b>
                  <br />
                  Цена транзакции: <b>{priceTransaction}</b>
                  <br />
                  Цена закрытия: <b>{priceClose}</b>
                  <br />
                  Выручка: <b>{proceed}</b>
                  <br />
                  Реализованная П/У: <b>{realizedPL}</b>
                  <br />
                  Рыноч. переоценка П/У: <b>{mtmPL}</b>
                  <br />
                </p>
              </Col>
            </Row>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  /**
   * Загрузить данные
   */
  load() {
    this.setState({
      loading: true,
    });
    TradeHelper.GetAll()
      .then((res) => {
        if (res.success) {
          this.setState({
            loading: false,
            trades: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        this.props.MessageSet(null, error.data, null);
        console.log(error);
      });
  }

  /**
   * Удалить сделку
   */
  remove() {
    this.setState({
      loading: true,
    });
    if (this.tradeRemove !== null) {
      var tradeRemove = this.state.tradeRemove;
      TradeHelper.Remove(tradeRemove.tradeId)
        .then((res) => {
          if (res.success) {
            this.setState({
              tradeRemove: null,
            });
          } else {
            this.props.MessageSet(null, res.data, null);
          }
        })
        .catch((error) => {
          this.props.MessageSet(null, error.data, null);
          console.log(error);
        })
        .then((e) => {
          this.load();
        });
    }
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load();
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {};
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Trades);
