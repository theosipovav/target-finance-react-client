import React, { Component } from "react";
class Page extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Выполняется до отрисовки компонента
   */
  componentWillMount() {
    //
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return <div>123</div>;
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}
export default Page;
