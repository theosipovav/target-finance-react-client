import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import styles from "./Home.module.scss";
import Jumbotron from "../../components/UI/Jumbotron/Jumbotron";
import ListSite from "../../components/UI/ListSite/ListSite";
import { Col, Row } from "react-bootstrap";
import imgYouCan1 from "../../img/YouCan1.png";
import imgYouCan2 from "../../img/YouCan2.png";
import imgYouCan3 from "../../img/YouCan3.png";

/**
 * Страница "Главная"
 */
const Home = (props) => {
  let isAuthenticated = props.isAuthenticated;
  if (isAuthenticated) {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <Jumbotron title={"Учет и контроль ваших инвестиций"}>
              <ListSite>
                <li>Добавляйте купленные акции и облигации</li>
                <li>Следите за портфелем</li>
                <li>Анализируйте доходность</li>
              </ListSite>
            </Jumbotron>
          </div>
          <div className="col-12 d-flex justify-content-center align-content-center">
            <Link to={"/import"} className="btn btn-primary btn-lg">
              Начать работу
            </Link>
          </div>
          <div className="col-12 text-center mt-2">
            <p className="lead">Вы можете выполнить импорт отчета брокера</p>
          </div>
        </div>
        <Row>
          <Col className="text-center text-primary">
            <h3 className="display-4 mt-3">ВЫ МОЖЕТЕ</h3>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-end align-items-center">
            <p className={styles.TextYouCan}>Создавать, редактировать и делиться портфелем</p>
          </Col>
          <Col className="d-flex justify-content-center align-items-center">
            <img className={styles.ImgYouCan} src={imgYouCan1} alt="" srcSet="" />
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-center align-items-center">
            <img className={styles.ImgYouCan} src={imgYouCan2} alt="" srcSet="" />
          </Col>
          <Col className="d-flex justify-content-start align-items-center">
            <p className={styles.TextYouCan}>Просматривать будущие и прошлые дивиденды в удобном формате</p>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-end align-items-center">
            <p className={styles.TextYouCan}>Выбирать облигации с наибольшей доходностью</p>
          </Col>
          <Col className="d-flex justify-content-center align-items-center">
            <img className={styles.ImgYouCan} src={imgYouCan3} alt="" srcSet="" />
          </Col>
        </Row>
      </React.Fragment>
    );
  } else {
    return (
      <div className={styles.sectionHello}>
        <h1>Учет и контроль ваших инвестиций</h1>
        <p className="lead">Добавляйте купленные акции и облигации, следите за портфелем, анализируйте доходность</p>
        <div className="d-flex flex-column  m-auto" style={{ maxWidth: "250px" }}>
          <Link to={"/signup"} className="btn btn-light">
            Регистрация
          </Link>
          <p className="lead mt-3 mb-1">Уже есть учетная запись?</p>
          <Link to={"/signin"} className="btn btn-outline-light">
            Авторизация
          </Link>
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.auth.user,
});
export default connect(mapStateToProps, null)(Home);
