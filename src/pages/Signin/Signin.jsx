import React, { Component } from "react";
import { connect } from "react-redux";
import { Navigate } from "react-router-dom";
import SigninForm from "../../components/Forms/SigninForm/SigninForm";

/**
 * Страница "Авторизация пользователя в системе"
 */
class Signin extends Component {
  state = {
    isAuthenticated: false,
  };

  render() {
    let isAuthenticated = this.props.isAuthenticated;

    if (isAuthenticated) {
      return <Navigate to="/" />;
    }

    return (
      <div>
        <div className="row">
          <div className="col-12 text-center">
            <h1 className="h1 text-white mt-1 mb-3">Учет и контроль ваших инвестиций</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <SigninForm />
          </div>
        </div>
      </div>
    );
  }
}

/**
 * Получает state из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.user,
  };
}

export default connect(mapStateToProps, null)(Signin);
