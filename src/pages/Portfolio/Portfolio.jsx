import React from "react";
import PortfolioSummary from "../../components/PortfolioSummary/PortfolioSummary";
import PortfolioList from "../../components/PortfolioList/PortfolioList";

/**
 * Инвестиционные портфели
 * @param {*} props
 * @returns
 */
const Portfolio = (props) => {
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-12">
          <h1 className="display-4 mt-3">Портфель акций</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <h2 className="display-5 mt-3">Активные портфели</h2>
        </div>
        <div className="col-12">
          <PortfolioList />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Portfolio;
