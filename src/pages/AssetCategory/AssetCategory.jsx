import React, { Component } from "react";
import styles from "./AssetCategory.module.scss";
import { connect } from "react-redux";
import AssetCategoryHelper from "../../helpers/AssetCategoryHelper";
import { MessageSet } from "../../store/actions/MessageAction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faObjectGroup, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Button, Col, Row, Table } from "react-bootstrap";
import Loader from "../../components/UI/Loader/Loader";
import AssetCategoryForm from "../../components/Forms/AssetCategoryForm/AssetCategoryForm";

class AssetCategory extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      loadingStep: 0,
      loadingMax: 1,
      assetCategories: [],
    };
  }

  /**
   * Загрузка данных
   */
  load() {
    AssetCategoryHelper.GetAll()
      .then((res) => {
        if (res.success) {
          var step = this.state.loadingStep + 1;
          console.log(res.data);
          this.setState({
            loadingStep: step,
            assetCategories: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      });
  }

  /**
   * Создать запись
   * @param {*} e
   */
  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ loadingStep: 0 });
    let form = e.target;
    let formData = new FormData(form);
    AssetCategoryHelper.Create(formData)
      .then((res) => {
        if (res.success) {
          this.load();
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response, null);
      });
  };

  /**
   * Удалить запись
   * @param {*} id
   */
  handleRemove = (id) => {
    this.setState({
      loadingStep: 0,
    });
    AssetCategoryHelper.Remove(id)
      .then((res) => {
        console.log(res);
        if (res.success) {
          this.load();
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response, null);
      });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;
    return (
      <React.Fragment>
        <Row>
          <Col>
            <h1 className="h3 mt-3">Управление классами актива</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <AssetCategoryForm handleSubmit={(e) => this.handleSubmit(e)} />
          </Col>
        </Row>
        <hr />
        <Row>
          <Col>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Наименование</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.state.assetCategories.map((obj, index) => {
                  console.log(obj);
                  return (
                    <tr key={index}>
                      <td>{index}</td>
                      <td>{obj.name}</td>
                      <td className="text-center">
                        <Button variant={"link"} className="text-danger" onClick={() => this.handleRemove(obj.assetCategoryId)}>
                          <FontAwesomeIcon icon={faTrash}></FontAwesomeIcon>
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load();
  }
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(null, mapDispatchToProps)(AssetCategory);
