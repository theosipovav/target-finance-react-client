import React, { Component } from "react";
import ImportForm from "../../components/Forms/ImportForm/ImportForm";

/**
 * Страница "Импорт сделки"
 */
class Import extends Component {
  state = {
    selectedFile: null,
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * render
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <h1 className="h1">Выполнить импорт сделки</h1>
          </div>
          <div className="row">
            <div className="col-12">
              <ImportForm />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  onFileChange = (event) => {
    this.setState({
      selectedFile: event.target.files[0],
    });
  };

  onFileUpload = () => {
    const form = new FormData();
    form.append("file", this.state.selectedFile, this.state.selectedFile.name);

    console.log("form", form);
    console.log({ state: this.state, props: this.props });
  };

  /**
   * componentDidMount
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default Import;
