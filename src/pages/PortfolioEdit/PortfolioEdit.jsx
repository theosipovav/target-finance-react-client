import { createBrowserHistory } from "history";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PortfolioCreateForm from "../../components/Forms/PortfolioCreateForm/PortfolioCreateForm";
import PortfolioForm from "../../components/Forms/PortfolioForm/PortfolioForm";
import Loader from "../../components/UI/Loader/Loader";
import PortfolioHelper from "../../helpers/PortfolioHelper";
import PortfolioTypesHelper from "../../helpers/PortfolioTypesHelper";
import TradeHelper from "../../helpers/TradeHelper";
import { MessageSet } from "../../store/actions/MessageAction";
import styles from "./PortfolioEdit.module.scss";
import { GenId } from "../../func";
import { Button } from "react-bootstrap";

/**
 * Страница "Редактирование инвестиционного портфеля"
 */
class PortfolioEdit extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    const history = createBrowserHistory();
    const arr = history.location.pathname.split("/");
    var id = false;
    arr.map((str, index) => {
      if (str == "edit") {
        id = parseInt(arr[index + 1]);
      }
    });

    this.state.id = id;
    this.state.loadingStep = 0;
    this.state.loadingMax = 2;
    this.state.portfolio = null;
    this.state.portfolioType = null;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      loadingStep: 0,
    });
    var form = e.target;
    var formData = new FormData(form);
    PortfolioHelper.UpdatePortfolio(formData)
      .then((res) => {
        if (res.success) {
          //
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      })
      .then(() => {
        this.load();
      });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;

    var idForm = GenId();
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <h1 className="h3 mt-3 mb-2">Редактирование портфеля "{this.state.portfolio.name}" </h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <PortfolioForm id={idForm} portfolio={this.state.portfolio} portfolioType={this.state.portfolioType} onSubmit={(e) => this.handleSubmit(e)} />
          </div>
          <div className="col-12">
            <div className="d-flex justify-content-center align-content-center">
              <Link to="/portfolio" className="btn btn-warning mx-1">
                Отмена
              </Link>
              <Button variant="primary" type="submit" form={idForm} className="mx-1">
                Сохранить
              </Button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  load() {
    this.setState({
      loadingStep: 0,
    });
    if (!this.state.id) {
      this.props.MessageSet(null, "Некорректный адрес", null);
    }

    PortfolioHelper.GetOne(this.state.id)
      .then((res) => {
        if (res.success) {
          var step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            portfolio: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      });

    PortfolioTypesHelper.GetAll()
      .then((res) => {
        if (res.success) {
          var step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            portfolioType: res.data,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      });
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load();
  }
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(null, mapDispatchToProps)(PortfolioEdit);
