import React, { Component } from "react";
import styles from "./About.module.scss";
import { Col, Row } from "react-bootstrap";

/**
 * Страница "О нас"
 */
class About extends Component {
  render() {
    return (
      <React.Fragment>
        <div className={styles.PageAbout}>
          <Row>
            <Col>
              <h1 className="h3 my-3">О сервисе</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <p>Сервис позволяет учитывать акции, облигации, ETF, валюты и другие виды активов.</p>
              <p>Предоставляет информацию и оценку эффективности ваших инвестиций в сравнении с доходностью индекса ММВБ, инфляции, ставок по депозитам.</p>
              <p>
                Основной единицей учета является сделка. Также для более точной оценки ваших инвестиций вы можете учитывать дивидендные, купонные и амортизационные выплаты, вести учет
                комиссий и расходов для каждого портфеля в отдельности. Сервисом удобно пользоваться как в web-версии, так и с мобильных устройств.
              </p>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}
export default About;
