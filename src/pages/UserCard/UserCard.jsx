import React, { Component } from "react";
import styles from "./UserCard.module.scss";
import { connect } from "react-redux";
import Loader from "../../components/UI/Loader/Loader";
import UsersHelper from "../../helpers/UsersHelper";
import { Col, Form, Row } from "react-bootstrap";
import { GenId } from "../../func";
import { Link } from "react-router-dom";
import iconUserCard from "../../img/user-card.png";

class UserCard extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      user: {},
      role: {},
    };
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (this.state.loading) return <Loader />;
    return (
      <React.Fragment>
        <Row>
          <Col>
            <h1 className="h3 mt-3">Личный кабинет</h1>
          </Col>
        </Row>
        <Row>
          <Col sm="5">
            <div className="d-flex justify-content-center align-items-center h-100">
              <img src={iconUserCard} alt={"logo"} className={styles.iconUserCard} />
            </div>
          </Col>
          <Col sm="7">
            <Form>
              <Form.Group as={Row} className="mb-3" controlId={GenId()}>
                <Form.Label column sm="4">
                  ФИО пользователя
                </Form.Label>
                <Col sm="8">
                  <Form.Control plaintext readOnly defaultValue={this.state.user.name} />
                </Col>
              </Form.Group>
              <Form.Group as={Row} className="mb-3" controlId={GenId()}>
                <Form.Label column sm="4">
                  Логин для входа в систему
                </Form.Label>
                <Col sm="8">
                  <Form.Control plaintext readOnly defaultValue={this.state.user.login} />
                </Col>
              </Form.Group>
              <Form.Group as={Row} className="mb-3" controlId={GenId()}>
                <Form.Label column sm="4">
                  Адрес электронной почты
                </Form.Label>
                <Col sm="8">
                  <Form.Control plaintext readOnly defaultValue={this.state.user.email} />
                </Col>
              </Form.Group>
              <Form.Group as={Row} className="mb-3" controlId={GenId()}>
                <Form.Label column sm="4">
                  Дата регистрации
                </Form.Label>
                <Col sm="8">
                  <Form.Control plaintext readOnly defaultValue={this.state.user.dtRegistration} />
                </Col>
              </Form.Group>
              <hr />
              <Form.Group as={Row} className="mb-3" controlId={GenId()}>
                <Form.Label column sm="4">
                  Роль
                </Form.Label>
                <Col sm="8">
                  <Form.Control plaintext readOnly defaultValue={this.state.role.name} />
                </Col>
              </Form.Group>
              <hr />
              <div className="d-flex justify-content-center align-items-center">
                <Link to={"/"} className="btn btn-primary">
                  Назад
                </Link>
              </div>
            </Form>
          </Col>
        </Row>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    console.log(this.props);
    this.setState({
      loading: false,
      user: this.props.user,
      role: this.props.role,
    });
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  let user = null;
  let role = null;
  if (!!state.auth.userJson) {
    user = JSON.parse(state.auth.userJson);
  }
  if (!!state.auth.roleJson) {
    role = JSON.parse(state.auth.roleJson);
  }
  return {
    isAuthenticated: !!state.auth.user,
    user: user,
    role: role,
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    //
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserCard);
