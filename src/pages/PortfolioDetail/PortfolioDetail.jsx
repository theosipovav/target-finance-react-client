import React, { Component } from "react";
import styles from "./PortfolioDetail.module.scss";
import { Link, Navigate } from "react-router-dom";
import { createBrowserHistory } from "history";
import Loader from "../../components/UI/Loader/Loader";

import { connect } from "react-redux";
import { MessageSet } from "../../store/actions/MessageAction";
import PortfolioHelper from "../../helpers/PortfolioHelper";
import PortfolioTypesHelper from "../../helpers/PortfolioTypesHelper";
import { Col, FloatingLabel, Form, Row, Table } from "react-bootstrap";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ChartTradeFromAssetCategory from "../../components/Charts/ChartTradeFromAssetCategory";
import ChartTradeFromSymbol from "../../components/Charts/ChartTradeFromSymbol";
import ChartVerticalBar from "../../components/Charts/ChartVerticalBar";
import TradeHelper from "../../helpers/TradeHelper";

/**
 * Страница "Подробная информация о б инвестиционном портфеле"
 */
class PortfolioDetail extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);

    const history = createBrowserHistory();
    const arr = history.location.pathname.split("/");
    let portfolioId = parseInt(arr[2]);

    this.state = {
      portfolioId: portfolioId,
      isError: false,
      loading: false,
      loadingStep: 0,
      loadingMax: 5,
      summaryFromAssetCategory: [],
      summaryFromSymbols: [],
      summaryByMonthForPrice: [],
    };
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (!!this.state.isError) return <Navigate to="/portfolio" />;
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;

    var portfolio = this.state.portfolio;
    var note = !!portfolio.note ? portfolio.note : "";

    return (
      <div className="portfolio-detail pt-3">
        <Row>
          <Col xs="10">
            <h1 className="h3 my-3">{portfolio.name}</h1>
          </Col>
          <Col xs="2">
            <div className="d-flex justify-content-end align-items-center">
              <Link to={`/portfolio/edit/${portfolio.portfolioId}`} className="btn btn-link btn-lg">
                <FontAwesomeIcon icon={faEdit} />
              </Link>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group>
              <Form.Label>Наименование инвестиционного портфеля</Form.Label>
              <Form.Control type="text" placeholder="Наименование инвестиционного портфеля" readOnly value={portfolio.name} />
            </Form.Group>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <Form.Group>
              <Form.Label>Тип портфеля</Form.Label>
              <Form.Control type="text" placeholder="Тип портфеля" readOnly value={portfolio.portfolioType.name} />
            </Form.Group>
          </Col>
          <Col>
            <Form.Group>
              <Form.Label>Дата открытия</Form.Label>
              <Form.Control type="text" placeholder="Дата открытия" readOnly value={portfolio.dtOpening} />
            </Form.Group>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <Form.Group>
              <Form.Label>Фиксированная комиссия</Form.Label>
              <Form.Control type="text" placeholder="Фиксированная комиссия" readOnly value={portfolio.commissionFixed} className="text-right" />
              <Form.Text className="text-muted">Для автоматического расчета комиссии при внесении сделок и импорте отчетов (если комиссии отсутствуют в отчете).</Form.Text>
            </Form.Group>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <Form.Group>
              <FloatingLabel label="Заметка">
                <Form.Control as="textarea" placeholder="Заметка" readOnly value={note} />
              </FloatingLabel>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <h3 className="h5 mt-3">Стоимость портфеля</h3>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="">
              <ChartVerticalBar data={this.state.summaryByMonthForPrice} title="Стоимость портфеля" />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="d-flex flex-column">
              <h3 className="h5 mt-3">Состав портфеля по активам</h3>
              <div className="">
                <this.renderTableFromAssetCategory />
              </div>
              <div className={styles.CharContainer}>
                <ChartTradeFromAssetCategory data={this.state.summaryFromAssetCategory} />
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="d-flex flex-column">
              <h3 className="h5 mt-3">Тикеры</h3>
            </div>
            <div className="">
              <this.renderTableFromSymbols />
            </div>
            <div className={styles.CharContainer}>
              <ChartTradeFromSymbol data={this.state.summaryFromSymbols} />
            </div>
          </Col>
        </Row>
      </div>
    );
  }

  renderTableFromAssetCategory = () => {
    var summary = this.state.summaryFromAssetCategory;
    return (
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>Актив</th>
            <th>Валюта</th>
            <th>Количество</th>
            <th>Средня цена транзакции</th>
            <th>Средня цена закрытия</th>
            <th>Средняя выручка</th>
          </tr>
        </thead>
        <tbody>
          {summary.map((obj, index) => {
            var assetCategoryName = !obj.assetCategory ? "" : obj.assetCategory.name;
            return (
              <tr key={index}>
                <td>{assetCategoryName}</td>
                <td>{obj.currency.symbol}</td>
                <td>{obj.count}</td>
                <td>{obj.priceTransaction}</td>
                <td>{obj.priceClose}</td>
                <td>{obj.proceed}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    );
  };

  renderTableFromSymbols = () => {
    var summary = this.state.summaryFromSymbols;
    return (
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>Тикер</th>
            <th>Валюта</th>
            <th>Количество</th>
            <th>Средня цена транзакции</th>
            <th>Средня цена закрытия</th>
            <th>Средняя выручка</th>
          </tr>
        </thead>
        <tbody>
          {summary.map((obj, index) => {
            var symbolValue = !obj.symbol ? "" : obj.symbol.value;
            var currencySymbol = !obj.currency ? "" : obj.currency.symbol;

            return (
              <tr key={index}>
                <td>{symbolValue}</td>
                <td>{currencySymbol}</td>
                <td>{obj.count}</td>
                <td>{obj.priceTransaction}</td>
                <td>{obj.priceClose}</td>
                <td>{obj.proceed}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    );
  };

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load(this.state.portfolioId);
  }

  load(id) {
    this.setState({
      loadingStep: 0,
    });
    PortfolioHelper.GetPortfolioById(id)
      .then((data) => {
        if (!!data) {
          let step = this.state.loadingStep + 1;
          let dt = new Date(data.dtOpening);
          data.dtOpening = dt.toISOString().substring(0, 10);
          this.setState({
            loadingStep: step,
            portfolio: data,
          });
        } else {
          console.log("loadPortfolio", data);
        }
      })
      .catch((res) => {
        console.log(res);
        this.props.MessageSet(null, null, null);
      });
    PortfolioTypesHelper.GetAll()
      .then((data) => {
        if (!!data) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            portfolioTypes: data,
          });
        } else {
          console.log("loadPortfolio", data);
        }
      })
      .catch((res) => {
        console.log(res);
        this.props.MessageSet(null, null, null);
      });
    PortfolioHelper.GetSummaryFromAssetCategory(id)
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            summaryFromAssetCategory: res.data,
          });
        } else {
          console.log(res.data);
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((res) => {
        console.log(res);
        this.props.MessageSet(null, null, null);
      });
    PortfolioHelper.GetSummaryFromSymbols(id)
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            summaryFromSymbols: res.data,
          });
        } else {
          console.log(res.data);
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((res) => {
        console.log(res);
        this.props.MessageSet(null, null, null);
      });

    TradeHelper.GetGroupByMonthForPrice(id)
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          const summary = {
            labels: [],
            data: [],
          };
          res.data.forEach((obj) => {
            var dt = new Date(obj.dt);
            var dtString = dt.toLocaleString("ru-RU");
            var price = 0;
            obj.trades.forEach((trade) => {
              price += trade.count * trade.priceTransaction;
            });
            summary.labels.push(dtString);
            summary.data.push(price);
          });
          this.setState({
            loadingStep: step,
            summaryByMonthForPrice: summary,
          });
        } else {
          console.log(res.data);
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((res) => {
        console.log(res);
        this.props.MessageSet(null, null, null);
      });
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    //
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortfolioDetail);
