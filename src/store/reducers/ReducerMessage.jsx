import { MESSAGE_SET, MESSAGE_UNSET } from "../actions/actionTypes";

const initialState = {
  isError: false,
  text: null,
  title: null,
  header: null,
};

export default function ReducerMessage(state = initialState, action) {
  switch (action.type) {
    case MESSAGE_SET:
      return {
        ...state,
        isError: action.isError,
        text: action.text,
        title: action.title,
        header: action.header,
      };
    case MESSAGE_UNSET:
      return {
        ...state,
        isError: false,
        text: null,
        title: null,
        header: null,
      };
    default:
      return state;
  }
}
