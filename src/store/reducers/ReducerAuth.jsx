import { AUTH_LOGOUT, AUTH_SUCCESS, AUTH_ERROR } from "../actions/actionTypes";

const initialState = {
  user: null,
  role: null,
  token: null,
  userJson: null,
  roleJson: null,
  authError: false,
};

export default function ReducerAuth(state = initialState, action) {
  switch (action.type) {
    case AUTH_SUCCESS:
      return {
        ...state,
        user: action.userId,
        role: action.roleId,
        token: action.token,
        userJson: action.userJson,
        roleJson: action.roleJson,
      };

    case AUTH_LOGOUT:
      return {
        ...state,
        user: null,
        role: null,
        token: null,
        userJson: null,
        roleJson: null,
      };
    case AUTH_ERROR:
      return {
        ...state,
        authError: true,
        user: null,
        role: null,
        token: null,
        userJson: null,
        roleJson: null,
      };
    default:
      return state;
  }
}
