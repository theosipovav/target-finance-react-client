import { combineReducers } from "redux";
import ReducerAuth from "./ReducerAuth";
import ReducerMessage from "./ReducerMessage";

export default combineReducers({
  auth: ReducerAuth,
  message: ReducerMessage,
});
