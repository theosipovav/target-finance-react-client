import axios from "axios";
import { AUTH_ERROR, AUTH_SUCCESS } from "./actionTypes";
import { logout } from "../../store/actions/ActionLogout";
import { URL_LOGIN } from "../../rest/urls";

/**
 * Авторизация в системе
 * @param {*} login Логин
 * @param {*} password Пароль
 * @returns
 */
export function ActionLogin(login, password) {
  return async (dispatch) => {
    let data = {
      login: login,
      password: password,
    };
    let response;
    try {
      response = await axios.post(URL_LOGIN, data);
      let userId = parseInt(response.data.user.userId);
      let roleId = parseInt(response.data.user.role.roleId);
      let userJson = JSON.stringify(response.data.user);
      let roleJson = JSON.stringify(response.data.user.role);
      let token = response.data.token;
      let timeSession = parseInt(response.data.expirationTime);
      let expirationTime = new Date(new Date().getTime() + timeSession).getTime();

      localStorage.setItem("user", userId);
      localStorage.setItem("role", roleId);
      localStorage.setItem("token", token);
      localStorage.setItem("userJson", userJson);
      localStorage.setItem("roleJson", roleJson);
      localStorage.setItem("expirationTime", expirationTime);
      dispatch(loginSuccess(userId, roleId, token, userJson, roleJson));
      dispatch(autoLogout(timeSession));
    } catch (error) {
      dispatch(loginError());
    }
    return response;
  };
}

/**
 * Успешная авторизация пользователя
 * @param {*} user
 * @returns
 */
export function loginSuccess(userId, roleId, token, userJson, roleJson) {
  return {
    type: AUTH_SUCCESS,
    userId,
    roleId,
    token,
    userJson,
    roleJson,
  };
}

/**
 * Авторизация прошла с ошибкой
 * @returns
 */
export function loginError() {
  return {
    type: AUTH_ERROR,
  };
}

/**
 * Автоматический выход
 */
export function autoLogout(time) {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(logout());
    }, time);
  };
}

export function autoLogin() {
  return (dispatch) => {
    try {
      const storage = {
        userId: parseInt(localStorage.getItem("user")),
        roleId: parseInt(localStorage.getItem("role")),
        token: localStorage.getItem("token"),
        userJson: localStorage.getItem("userJson"),
        roleJson: localStorage.getItem("roleJson"),
        expirationTime: localStorage.getItem("expirationTime"),
      };
      if (!storage.userId || !storage.roleId || !storage.token || !storage.userJson || !storage.roleJson) {
        dispatch(logout());
      } else {
        const expirationTime = storage.expirationTime;
        const nowTime = new Date().getTime();
        if (expirationTime <= nowTime) {
          dispatch(logout());
        } else {
          dispatch(loginSuccess(storage.userId, storage.roleId, storage.token, storage.userJson, storage.roleJson));
          dispatch(autoLogout(1 * 1000 * 60 * 60));
        }
      }
    } catch (error) {
      dispatch(logout());
    }
  };
}
