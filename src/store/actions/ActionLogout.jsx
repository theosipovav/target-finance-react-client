import axios from "axios";
import { AUTH_LOGOUT } from "./actionTypes";

/**
 * Выход из системы
 * @returns
 */
export function logout() {
  console.log("Выполнен выход из системы");
  localStorage.removeItem("user");
  localStorage.removeItem("role");
  localStorage.removeItem("expirationTime");
  localStorage.removeItem("token");
  localStorage.removeItem("userJson");
  localStorage.removeItem("roleJson");
  return {
    type: AUTH_LOGOUT,
  };
}
