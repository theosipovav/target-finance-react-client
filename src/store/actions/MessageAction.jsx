import { MESSAGE_SET, MESSAGE_UNSET } from "../actions/actionTypes";

/**
 * Авторизация в системе
 * @param {*} login Логин
 * @param {*} password Пароль
 * @returns
 */
export function MessageSet(isError, text, title, header) {
  return async (dispatch) => {
    dispatch({ type: MESSAGE_SET, isError, text, title, header });
  };
}
export function unsetMessage() {
  return async (dispatch) => {
    dispatch({ type: MESSAGE_UNSET });
  };
}
