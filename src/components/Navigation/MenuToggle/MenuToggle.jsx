import React from "react";
import "bootstrap";
import * as bootstrap from "bootstrap";
import styles from "MenuToggle.Module.scss";

const MenuToggle = (props) => {
  const cls = ["fa"];

  return <i className={cls.join(" ")} onClick={props.onToggle}></i>;
};

export default MenuToggle;
