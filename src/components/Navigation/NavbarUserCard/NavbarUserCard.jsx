import React, { Component } from "react";
import styles from "./NavbarUserCard.module.scss";
import { connect } from "react-redux";
import axios from "axios";
import { URL_GET_USER } from "../../../rest/urls";
import { Button, Dropdown, Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export class NavbarUserCard extends Component {
  state = {
    user: null,
    role: null,
    helloString: "",
  };

  render() {
    const isAuthenticated = this.props.isAuthenticated;
    if (isAuthenticated) {
      let user = this.props.user;
      return (
        <React.Fragment>
          <Dropdown>
            <Dropdown.Toggle split variant="light" className={styles.DropdownToggleUser}>
              <span className="mx-2">{user.name}</span>
            </Dropdown.Toggle>
            <Dropdown.Menu className="super-colors">
              <Dropdown.Item>
                <Link to={"/user-card"} className="btn btn-link text-decoration-none text-dark">
                  Личный кабинет
                </Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to={"/logout"} className="btn btn-link text-decoration-none text-dark">
                  Выход
                </Link>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </React.Fragment>
      );
    } else {
      return <div className="text-white">Здравствуйте, рады видеть Вас!</div>;
    }
  }
}

function mapStateToProps(state) {
  let user = null;
  let role = null;
  if (!!state.auth.userJson) {
    user = JSON.parse(state.auth.userJson);
  }
  if (!!state.auth.roleJson) {
    role = JSON.parse(state.auth.roleJson);
  }
  return {
    isAuthenticated: !!state.auth.user,
    user: user,
    role: role,
  };
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(NavbarUserCard);
