import React from "react";
import { connect } from "react-redux";
import { Route, NavLink } from "react-router-dom";

import "./Navbar.module.scss";

class Navbar extends React.Component {
  user = true;

  links = {
    guest: [
      { text: "Главная", to: "." },
      { text: "Авторизация", to: "/signin" },
      { text: "Регистрация", to: "/signup" },
      { text: "О нас", to: "about" },
    ],
    user: [
      { text: "Главная", to: "." },
      { text: "Портфели", to: "portfolio" },
      { text: "Сделки", to: "/trades" },
      { text: "Импорт данных", to: "import" },
      { text: "О нас", to: "about" },
      { text: "Выход", to: "logout" },
    ],
    admin: [
      { text: "Главная", to: "." },
      { text: "Управление пользователями", to: "/user-control" },
      { text: "Упр. валютой", to: "/currency" },
      { text: "Упр. классами актива", to: "/assetCategory" },
      { text: "Упр. статусами сделок", to: "/status" },
      { text: "О нас", to: "about" },
      { text: "Выход", to: "logout" },
    ],
  };

  render() {
    let isAuthenticated = this.props.isAuthenticated;

    let user = null;
    let role = null;
    let links = [];
    if (isAuthenticated) {
      user = this.props.user;
      role = this.props.role;
      if (role.roleId == 1 || role.name == "Администратор") {
        links = this.links.admin;
      } else {
        links = this.links.user;
      }
    } else {
      links = this.links.guest;
    }

    return (
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        {links.map((link, index) => {
          return (
            <NavLink key={index} className="nav-link" aria-current="page" to={link.to}>
              {link.text}
            </NavLink>
          );
        })}
      </ul>
    );
  }
}

function mapStateToProps(state) {
  let user = null;
  let role = null;
  if (!!state.auth.userJson) {
    user = JSON.parse(state.auth.userJson);
  }
  if (!!state.auth.roleJson) {
    role = JSON.parse(state.auth.roleJson);
  }
  return {
    isAuthenticated: !!state.auth.user,
    user: user,
    role: role,
  };
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
