import React, { Component } from "react";
import { CloseButton, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { unsetMessage } from "../../store/actions/MessageAction";
import styles from "./MessageError.module.scss";

/**
 * Карточка с сообщением
 * Параметры:
 * status = (success || warning || error)
 * header
 * title
 * message
 *
 */
class MessageError extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  handleClose = () => {
    this.props.unsetMessage();
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (!this.props.message.isError) return null;

    let text = !!this.props.message.text ? this.props.message.text : "Пожалуйста, повторите попытку позже";
    let title = !!this.props.message.title ? this.props.message.title : "Ошибка при обращение к серверу";
    let header = !!this.props.message.header ? this.props.message.header : "Ошибка";

    return (
      <div className="card text-white bg-danger mt-3 mb-3">
        <div className="card-header d-flex justify-content-center align-items-center">
          <div className="flex-grow-1">{header}</div>
          <div className="">
            <CloseButton variant="white" />
          </div>
        </div>
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text text-wrap">{text}</p>
          <div className="d-flex justify-content-center align-items-center">
            <Button variant="light" onClick={(e) => this.handleClose()}>
              Закрыть
            </Button>
          </div>
        </div>
      </div>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

function mapStateToProps(state) {
  return {
    message: state.message,
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    unsetMessage: () => dispatch(unsetMessage()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MessageError);
