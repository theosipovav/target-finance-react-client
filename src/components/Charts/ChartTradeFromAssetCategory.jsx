import React from "react";
import styles from "./Chart.module.scss";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";

const ChartTradeFromAssetCategory = (props) => {
  var summary = props.data;
  ChartJS.register(ArcElement, Tooltip, Legend);
  var labels = [];
  var values = [];
  var backgroundColor = [];
  var borderColor = [];
  summary.map((obj, index) => {
    labels.push(obj.assetCategory.name);
    values.push(obj.count);
    backgroundColor.push(getRandomColor());
    borderColor.push("#FFF");
  });
  const data = {
    labels: labels,
    datasets: [
      {
        label: "# of Votes",
        data: values,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        borderWidth: 2,
      },
    ],
  };
  return <Pie data={data} />;
};

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
export default ChartTradeFromAssetCategory;
