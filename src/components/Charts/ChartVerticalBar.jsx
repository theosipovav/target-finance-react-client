import React from "react";
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

const ChartVerticalBar = (props) => {
  const colors = [];
  props.data.labels.forEach((element) => {
    colors.push(getRandomColor());
  });

  var options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: props.title,
      },
    },
  };
  var data = {
    labels: props.data.labels,
    datasets: [
      {
        label: props.title,
        data: props.data.data,
        backgroundColor: getRandomColor(),
      },
    ],
  };

  return <Bar options={options} data={data} />;
};

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export default ChartVerticalBar;
