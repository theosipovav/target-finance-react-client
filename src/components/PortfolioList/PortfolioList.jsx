import React, { Component } from "react";
import styles from "./PortfolioList.module.scss";
import { connect } from "react-redux";
import axios from "axios";
import { GET_PORTFOLIOS_USER, DELETE_PORTFOLIO_ID } from "../../rest/urls";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import CardPortfolio from "../UI/CardPortfolio/CardPortfolio";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

import { useState } from "react";
import { Button, Modal } from "react-bootstrap";

/**
 * Список портфеля пользователя
 */
export class PortfolioList extends Component {
  state = {
    portfolioList: [],
    isShowModal: false,
    portfolioDelete: {
      id: -1,
      name: "...",
    },
  };

  /**
   *
   */
  componentDidMount() {
    this.getPortfolioList();
  }

  /**
   * Получить список всех портфелей пользователя
   */
  getPortfolioList() {
    const headers = {
      Accept: "application/json",
      Authorization: "Bearer " + this.props.token,
    };

    axios
      .get(GET_PORTFOLIOS_USER, { headers: headers })
      .then((response) => {
        let portfolios = response.data;
        this.setState({
          loading: false,
          portfolioList: portfolios,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  /**
   * Закрыть модальное окно удаление портфеля
   */
  handleModalClose = () => {
    this.setState({
      isShowModal: false,
    });
  };

  /**
   * Подтверждение удаление выбранного портфеля в модальном окне
   */
  handleModalDelete = () => {
    const headers = {
      Accept: "application/json",
      Authorization: "Bearer " + this.props.token,
    };
    if (this.state.portfolioDelete.id == -1) {
      console.log("Не задан портфель на удаление");
    }
    axios
      .delete(DELETE_PORTFOLIO_ID + "/" + this.state.portfolioDelete.id, { headers: headers })
      .then((response) => {
        this.setState({
          isShowModal: false,
          portfolioDelete: {
            id: -1,
            name: "...",
          },
        });
        this.getPortfolioList();
      })
      .catch((error) => {
        this.setState({
          isShowModal: false,
          portfolioDelete: {
            id: -1,
            name: "...",
          },
        });
        console.log(error);
        console.log(error.response);
      });
  };

  onDeleteCard = (id, name) => {
    this.setState({
      isShowModal: true,
      portfolioDelete: {
        id: id,
        name: name,
      },
    });
  };

  render() {
    const portfolioList = this.state.portfolioList;
    let clsCardAdd = ["card", styles.PortfolioCardAdd];
    return (
      <React.Fragment>
        <div className="row">
          {portfolioList.map((p, index) => {
            return (
              <div key={index} className="col-md-4">
                <CardPortfolio
                  id={p.portfolioId}
                  name={p.name}
                  type={p.portfolioType.name}
                  dt={p.dtOpening}
                  note={p.note}
                  onDelete={(e) => {
                    this.onDeleteCard(p.portfolioId, p.name);
                  }}
                />
              </div>
            );
          })}
          <div className="col-md-4">
            <Link to={"/portfolio/create"} className={clsCardAdd.join(" ")}>
              <FontAwesomeIcon size="lg" icon={faPlus} color="#3f79d5" />
            </Link>
          </div>
        </div>

        <Modal show={this.state.isShowModal}>
          <Modal.Header closeButton>
            <Modal.Title>Удаление портфеля "{this.state.portfolioDelete.name}"</Modal.Title>
          </Modal.Header>
          <Modal.Body>Вы действительно хотите удалить портфель "{this.state.portfolioDelete.name}"?</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleModalClose}>
              Отмена
            </Button>
            <Button variant="danger" onClick={this.handleModalDelete}>
              Удалить
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    user: JSON.parse(state.auth.userJson),
    role: JSON.parse(state.auth.roleJson),
    token: state.auth.token,
  };
}

export default connect(mapStateToProps, null)(PortfolioList);
