import React, { Component } from "react";
import styles from "./PortfolioCreateForm.module.scss";
import StylesForm from "../Forms.module.scss";
import { connect } from "react-redux";

import axios from "axios";
import { GET_PORTFOLIO_TYPE, POST_CREATE_PORTFOLIO } from "../../../rest/urls";
import { SubmitForm } from "../../UI/SubmitForm/SubmitForm";

import { DateForm, NumberForm, TextareaForm, SelectForm, TextForm, ControlValidate } from "../../UI/ControlsForm/ControlsForm";
import { Link } from "react-router-dom";
import CardMessage from "../../UI/CardMessage/CardMessage";
import PortfolioHelper from "../../../helpers/PortfolioHelper";
import PortfolioTypesHelper from "../../../helpers/PortfolioTypesHelper";
import { MessageSet } from "../../../store/actions/MessageAction";

class PortfolioCreateForm extends Component {
  state = {
    loading: true,
    status: null,
    message: null,
    isFormLoading: false,
    isFormValid: false,
    portfolioTypes: [],
    formControls: {
      name: {
        name: "Name",
        value: "",
        label: "Наименование",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      portfolioType: {
        name: "Type",
        value: "",
        label: "Тип портфеля",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      commissionFixed: {
        name: "CommissionFixed",
        value: "",
        step: "0.0001",
        label: "Фиксированная комиссия",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      dtOpening: {
        name: "DtOpening",
        value: "",
        label: "Дата открытия",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      note: {
        name: "Note",
        value: "",
        type: "textarea",
        label: "Заметка",
        textError: "...",
        valid: false,
        touched: false,
        validation: {},
      },
    },
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отслеживание изменений ввода в элемент формы
   */
  onChangeHandler = (e, controlName) => {
    // Получение данных о элементах ввода формах
    const formControls = { ...this.state.formControls };
    const control = { ...formControls[controlName] };
    // Проверка валидации
    control.value = e.target.value;
    control.touched = true;
    control.valid = ControlValidate(e.target, control.validation);
    let isFormValid = true;
    formControls[controlName] = control;
    Object.keys(formControls).forEach((controlName) => {
      isFormValid = formControls[controlName].valid && isFormValid;
    });
    // Сохранение в state
    this.setState({
      isFormValid,
      formControls,
    });
  };

  /**
   * Отправка формы
   * @param {*} e
   */
  async handleSubmit(event) {
    event.preventDefault();

    if (!this.state.isFormValid) {
      this.setState({
        loading: false,
        status: "warning",
        message: "Заполните все необходимые поля",
      });
      return;
    }

    this.setState({
      loading: true,
    });

    const form = event.target;
    const formData = new FormData(form);
    const headers = {
      Accept: "application/json",
      Authorization: "Bearer " + this.props.token,
    };

    try {
      axios
        .post(POST_CREATE_PORTFOLIO, formData, { headers: headers })
        .then((response) => {
          let newState = {
            loading: false,
            status: null,
            message: null,
          };
          newState.status = !!response.data.status ? response.data.status : "error";
          newState.status = (newState.status + "").toLowerCase();
          newState.message = "";
          switch (newState.status) {
            case "success":
              newState.status = "success";
              newState.message = "Портфель создан";
              console.log(form);
              form.reset();

              break;
            case "warning":
              newState.status = "warning";
              newState.message = !!response.data.message ? response.data.message : "Подробности на сервере.";
              break;
            default:
              newState.status = "error";
              newState.message = !!response.data.message ? response.data.message : "Ошибка при создание нового портфеля. Подробности на сервере.";
              break;
          }

          this.setState(newState);
        })
        .catch((error) => {
          this.setState({
            loading: false,
            status: "error",
            message: "Ошибка сервера. Пожалуйста, повторите действие позже.",
          });
          console.log(error);
        });
    } catch (error) {
      this.setState({
        loading: false,
        status: "error",
        message: "Ошибка сервера. Пожалуйста, повторите действие позже.",
      });
      console.log(error);
    }
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    let clsForm = [];
    if (this.state.isFormLoading) {
      //
    }

    let options = [];
    this.state.portfolioTypes.map((obj, index) => {
      options.push({
        value: obj.portfolioTypeId,
        text: obj.name,
      });
    });

    return (
      <React.Fragment>
        {!!this.state.status ? <CardMessage message={this.state.message} status={this.state.status}></CardMessage> : null}

        <form
          className={clsForm.join(" ")}
          onSubmit={(e) => {
            this.handleSubmit(e);
          }}
        >
          <div className={styles.signupFormBlock}>
            <TextForm
              label={this.state.formControls.name.label}
              name={this.state.formControls.name.name}
              textHelp={this.state.formControls.name.textHelp}
              valid={this.state.formControls.name.valid}
              touched={this.state.formControls.name.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "name");
              }}
            />

            <SelectForm
              label={this.state.formControls.portfolioType.label}
              name={this.state.formControls.portfolioType.name}
              valid={this.state.formControls.portfolioType.valid}
              touched={this.state.formControls.portfolioType.touched}
              shouldValidate={true}
              options={JSON.stringify(options)}
              onChange={(event) => {
                this.onChangeHandler(event, "portfolioType");
              }}
            />

            <NumberForm
              label={this.state.formControls.commissionFixed.label}
              name={this.state.formControls.commissionFixed.name}
              valid={this.state.formControls.commissionFixed.valid}
              touched={this.state.formControls.commissionFixed.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "commissionFixed");
              }}
              step={this.state.formControls.commissionFixed.step}
            />
            <DateForm
              label={this.state.formControls.dtOpening.label}
              name={this.state.formControls.dtOpening.name}
              valid={this.state.formControls.dtOpening.valid}
              touched={this.state.formControls.dtOpening.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "dtOpening");
              }}
            />
            <TextareaForm
              label={this.state.formControls.note.label}
              name={this.state.formControls.note.name}
              valid={this.state.formControls.note.valid}
              touched={this.state.formControls.note.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "note");
              }}
            />
            <div className="row">
              <div className="col-12">
                <div className="d-flex justify-content-center align-content-center my-3">
                  <button type="submit" className="btn btn-primary mx-2" disabled={!this.state.isFormValid} style={{ width: "175px" }}>
                    Создать
                  </button>
                  <Link to={"/portfolio"} className="btn btn-warning color-primary mx-2" style={{ width: "175px" }}>
                    Назад
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    PortfolioTypesHelper.GetAll()
      .then((res) => {
        this.setState({
          loading: false,
          portfolioTypes: res.data,
        });
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      });
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    user: JSON.parse(state.auth.userJson),
    role: JSON.parse(state.auth.roleJson),
    token: state.auth.token,
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortfolioCreateForm);
