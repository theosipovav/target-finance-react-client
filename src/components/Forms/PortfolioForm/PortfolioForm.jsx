import React, { Component } from "react";
import styles from "./PortfolioForm.module.scss";
import StylesForm from "../Forms.module.scss";
import { connect } from "react-redux";

import axios from "axios";
import { GET_PORTFOLIO_TYPE, POST_CREATE_PORTFOLIO } from "../../../rest/urls";
import { SubmitForm } from "../../UI/SubmitForm/SubmitForm";

import { DateForm, NumberForm, TextareaForm, SelectForm, TextForm, ControlValidate } from "../../UI/ControlsForm/ControlsForm";
import { Link } from "react-router-dom";
import CardMessage from "../../UI/CardMessage/CardMessage";
import PortfolioHelper from "../../../helpers/PortfolioHelper";
import PortfolioTypesHelper from "../../../helpers/PortfolioTypesHelper";
import { MessageSet } from "../../../store/actions/MessageAction";
import { Button, Col, Container, FloatingLabel, Form, Row } from "react-bootstrap";
import { GenId } from "../../../func";

/**
 *
 * @param {*} props
 * @returns
 */
const PortfolioForm = (props) => {
  console.log(props);
  var portfolio = props.portfolio;
  var portfolioType = props.portfolioType;

  var defaultPortfolioId = !portfolio ? 0 : portfolio.portfolioId;
  var defaultName = !portfolio ? "" : portfolio.name;
  var defaultType = !portfolio ? null : portfolio.portfolioType.portfolioTypeId;
  var defaultCommissionFixed = !portfolio ? 0 : portfolio.commissionFixed;

  if (!portfolio) {
    var dt = new Date();
  } else {
    var dt = new Date(portfolio.dtOpening);
  }
  var month = dt.getMonth() + 1;
  var day = dt.getDate();
  if (month < 10) month = "0" + month;
  if (day < 10) day = "0" + day;
  var defaultDtOpening = dt.getFullYear() + "-" + month + "-" + day;
  var defaultNote = !portfolio ? 0 : portfolio.note;

  return (
    <Form id={props.id} onSubmit={props.onSubmit}>
      <input type="hidden" name="PortfolioId" defaultValue={defaultPortfolioId} />
      <Form.Group className="mb-3" controlId={GenId()}>
        <Row>
          <Col md="4">
            <Form.Label>Наименование</Form.Label>
          </Col>
          <Col md="8">
            <Form.Control type="text" name="Name" placeholder="Наименование" defaultValue={defaultName} />
          </Col>
          <Col md="12">
            <Form.Text className="text-muted">Введите наименование портфеля</Form.Text>
          </Col>
        </Row>
      </Form.Group>
      <Form.Group className="mb-3" controlId={GenId()}>
        <Row>
          <Col md="4">
            <Form.Label>Тип портфеля</Form.Label>
          </Col>
          <Col md="8">
            <Form.Select name="Type" aria-label="Тип портфеля" name="PortfolioTypeId" defaultValue={defaultType}>
              <option disabled>Тип портфеля</option>
              {portfolioType.map((obj, index) => {
                return (
                  <option key={index} value={obj.portfolioTypeId}>
                    {obj.name}
                  </option>
                );
              })}
            </Form.Select>
          </Col>
          <Col md="12">
            <Form.Text className="text-muted">Выберите тип портфеля</Form.Text>
          </Col>
        </Row>
      </Form.Group>
      <Form.Group className="mb-3" controlId={GenId()}>
        <Row>
          <Col md="4">
            <Form.Label>Фиксированная комиссия</Form.Label>
          </Col>
          <Col md="8">
            <Form.Control type="number" step={"0.0001"} name="CommissionFixed" defaultValue={defaultCommissionFixed} placeholder="Фиксированная комиссия" />
          </Col>
          <Col md="12">
            <Form.Text className="text-muted">Для автоматического расчета комиссии при внесении сделок и импорте отчетов (если комиссии отсутствуют в отчете).</Form.Text>
          </Col>
        </Row>
      </Form.Group>
      <Form.Group className="mb-3" controlId={GenId()}>
        <Row>
          <Col md="4">
            <Form.Label>Дата открытия</Form.Label>
          </Col>
          <Col md="8">
            <Form.Control type="date" name="DtOpening" placeholder="Дата открытия" defaultValue={defaultDtOpening} />
          </Col>
          <Col md="12">
            <Form.Text className="text-muted">Выберите дату открытие портфеля</Form.Text>
          </Col>
        </Row>
      </Form.Group>
      <Form.Group className="mb-3" controlId={GenId()}>
        <Row>
          <Col md="12">
            <FloatingLabel controlId={GenId()} label="Заметка">
              <Form.Control as="textarea" name="Note" placeholder="Заметка" defaultValue={defaultNote} style={{ height: "100px" }} />
            </FloatingLabel>
          </Col>
        </Row>
      </Form.Group>
    </Form>
  );
};

export default PortfolioForm;
