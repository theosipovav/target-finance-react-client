import React from "react";
import { Form, Row, Col, Button } from "react-bootstrap";
import { GenId } from "../../../func";

const StatusForm = (props) => {
  return (
    <Form onSubmit={(e) => props.handleSubmit(e)}>
      <Row>
        <Col md={5}>
          <Form.Group as={Col} controlId={GenId()}>
            <Form.Label>Наименование</Form.Label>
            <Form.Control type="text" name="name" placeholder="Наименование" required />
          </Form.Group>
        </Col>
        <Col md={4}>
          <Form.Group as={Col} controlId={GenId()}>
            <Form.Label>Описание</Form.Label>
            <Form.Control type="text" name="desc" maxLength={3} placeholder="Описание" required />
          </Form.Group>
        </Col>
        <Col md={3} className="d-flex justify-content-center align-items-end">
          <Button variant="primary" type="submit">
            Добавить
          </Button>
        </Col>
      </Row>
    </Form>
  );
};

export default StatusForm;
