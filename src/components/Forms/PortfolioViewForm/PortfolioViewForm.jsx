import React, { Component } from "react";
import styles from "./PortfolioViewForm.module.scss";
import { connect } from "react-redux";
import { Form, Row, Col, FloatingLabel, Button } from "react-bootstrap";
import PortfolioHelper from "../../../helpers/PortfolioHelper";
import PropTypes from "prop-types";
import Loader from "../../UI/Loader/Loader";
import PortfolioTypesHelper from "../../../helpers/PortfolioTypesHelper";
import { MessageSet } from "../../../store/actions/MessageAction";

/**
 * id Идентификатор портфеля
 */
class PortfolioViewForm extends Component {
  idForm = null;

  state = {
    loading: false,
    loadingStep: 0,
    loadingMax: 2,
    isReadonly: true,
    portfolio: null,
    portfolioTypes: null,
  };

  static propTypes = {
    id: PropTypes.number,
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.idForm = `${"Id"}${Math.random()}`;
  }

  loadPortfolio(id) {
    this.setState({
      loadingStep: 0,
    });

    PortfolioHelper.GetPortfolioById(id).then((data) => {
      if (!!data) {
        let step = this.state.loadingStep + 1;
        let dt = new Date(data.dtOpening);
        data.dtOpening = dt.toISOString().substring(0, 10);
        this.setState({
          loadingStep: step,
          portfolio: data,
        });
      } else {
        console.log("loadPortfolio", data);
      }
    });

    PortfolioTypesHelper.GetAll().then((data) => {
      if (!!data) {
        let step = this.state.loadingStep + 1;
        this.setState({
          loadingStep: step,
          portfolioTypes: data,
        });
      } else {
        console.log("loadPortfolio", data);
      }
    });
  }

  handleEdit = (e) => {
    let isReadonly = this.state.isReadonly ? false : true;
    this.setState({
      isReadonly: isReadonly,
    });
  };

  handleCancel = (e) => {
    this.handleEdit(e);
    this.loadPortfolio(this.state.portfolio.portfolioId);
  };

  handleSave = (e) => {
    e.preventDefault();

    const form = e.target;
    const formData = new FormData(form);

    const data = {
      PortfolioId: this.state.portfolio.portfolioId,
      Name: formData.get("Name"),
      CommissionFixed: formData.get("CommissionFixed"),
      DtOpening: formData.get("DtOpening"),
      Note: formData.get("Note"),
      PortfolioTypeId: formData.get("PortfolioTypeId"),
    };

    PortfolioHelper.UpdatePortfolio(data)
      .then((data) => {
        console.log(data);
        if (!!data) {
          this.handleEdit(null);
        } else {
          this.props.MessageSet("Пожалуйста, повторите попытку позже", "Ошибка при обращение к серверу", null);
        }
      })
      .catch((error) => {
        this.props.MessageSet("Пожалуйста, повторите попытку позже", "Ошибка при обращение к серверу", null);
        return false;
      });
  };

  handleChange(e) {
    try {
      const portfolio = this.state.portfolio;
      portfolio[e.target.name] = e.target.value;
      this.setState({
        portfolio: portfolio,
      });
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;

    return (
      <React.Fragment>
        <div className="d-flex justify-content-end align-items-center mb-3">
          {this.state.isReadonly ? (
            <React.Fragment>
              <Button variant="secondary" onClick={this.handleEdit}>
                Редактировать
              </Button>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Button variant="secondary" onClick={this.handleCancel} className="me-1">
                Отмена
              </Button>
              <Button variant="success" type="submit" form={this.idForm}>
                Сохранить
              </Button>
            </React.Fragment>
          )}
        </div>
        <Form id={this.idForm} onSubmit={(e) => this.handleSave(e)}>
          <FloatingLabel label="Наименование" className="mb-3">
            {this.state.isReadonly ? (
              <Form.Control type="text" placeholder="Наименование" defaultValue={this.state.portfolio.name} readOnly />
            ) : (
              <Form.Control type="text" placeholder="Наименование" name="Name" />
            )}
          </FloatingLabel>

          {this.state.isReadonly ? (
            <FloatingLabel label="Портфель" className="mb-3">
              <Form.Control type="text" placeholder="Портфель" defaultValue={this.state.portfolio.portfolioType.name} readOnly />
            </FloatingLabel>
          ) : (
            <FloatingLabel label="Портфель" className="mb-3">
              <Form.Select aria-label="Выберите тип портфеля" placeholder="Тип портфеля" name="PortfolioTypeId" defaultValue={this.state.portfolio.portfolioType.portfolioTypeId}>
                <option>Выберите тип портфеля</option>
                {this.state.portfolioTypes.map((obj, index) => {
                  return (
                    <option key={index} value={obj.portfolioTypeId}>
                      {obj.name}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>
          )}

          <FloatingLabel label="Фиксированная комиссия" className="mb-3">
            {this.state.isReadonly ? (
              <Form.Control type="text" placeholder="Фиксированная комиссия" defaultValue={this.state.portfolio.commissionFixed} readOnly />
            ) : (
              <Form.Control type="text" placeholder="Фиксированная комиссия" name="CommissionFixed" />
            )}
          </FloatingLabel>

          <FloatingLabel label="Дата открытия" className="mb-3">
            {this.state.isReadonly ? (
              <Form.Control type="date" placeholder="Дата открытия" defaultValue={this.state.portfolio.dtOpening} readOnly />
            ) : (
              <Form.Control type="date" placeholder="Дата открытия" name="DtOpening" />
            )}
          </FloatingLabel>

          <FloatingLabel label="Заметка" className="mb-3">
            {this.state.isReadonly ? (
              <Form.Control as="textarea" placeholder="Заметка" defaultValue={this.state.portfolio.note} readOnly />
            ) : (
              <Form.Control as="textarea" placeholder="Заметка" name="Note" />
            )}
          </FloatingLabel>
        </Form>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    if (!!this.props.id) {
      this.loadPortfolio(this.props.id);
    }
  }
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

// Создаем новый компонент, который "подключен" к роутеру
// const ShowTheLocationWithRouter = withRouter(ShowTheLocation);

export default connect(null, mapDispatchToProps)(PortfolioViewForm);
