import React, { Component } from "react";
import styles from "./TradeForm.module.scss";
import { connect } from "react-redux";
import { Form, Container, Row, Col, FloatingLabel, Button, ToastBody } from "react-bootstrap";
import { MessageSet } from "../../../store/actions/MessageAction";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

/**
 * id
 *
 * Режимы (mode): readonly, edit, create
 */
class TradeForm extends Component {
  idForm = null;

  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);

    this.state = {
      id: props.id,
      portfolios: props.portfolios,
      assetCategories: props.assetCategories,
      currencies: props.currencies,
      statuses: props.statuses,
      symbols: props.symbols,
      trade: props.trade,
    };
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var portfolios = this.state.portfolios;
    var assetCategories = this.state.assetCategories;
    var currencies = this.state.currencies;
    var symbols = this.state.symbols;
    var statuses = this.state.statuses;
    var trade = this.state.trade;

    var defaultPortfolioId = !trade ? portfolios[0].portfolioId : trade.portfolio.portfolioTypeId;
    var defaultAssetCategoryId = !trade ? assetCategories[0].assetCategoryId : trade.assetCategory.assetCategoryId;
    var defaultCurrencyId = !trade ? currencies[0].currencyId : trade.currency.currencyId;
    var defaultSymbolId = !trade ? symbols[0].symbolId : trade.symbol.symbolId;
    var defaultStatusId = !trade ? statuses[0].statusId : trade.status.statusId;

    if (!trade) {
      var dt = new Date();
    } else {
      var dt = new Date(trade.dt);
    }
    var month = dt.getMonth() + 1;
    var day = dt.getDate();
    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;
    var defaultDt = dt.getFullYear() + "-" + month + "-" + day;

    var defaultTradeId = !trade ? 0 : trade.tradeId;
    var defaultCount = !trade ? 0 : trade.count;
    var defaultPriceTransaction = !trade ? 0 : trade.priceTransaction;
    var defaultPriceClose = !trade ? 0 : trade.priceClose;
    var defaultProceed = !trade ? 0 : trade.proceed;
    var defaultCommission = !trade ? 0 : trade.commission;
    var defaultBasis = !trade ? 0 : trade.basis;
    var defaultRealizedPL = !trade ? 0 : trade.realizedPL;
    var defaultMtmPL = !trade ? 0 : trade.mtmPL;

    return (
      <React.Fragment>
        <Form id={this.state.id} onSubmit={(e) => this.props.handleSubmit(e)}>
          <input type="hidden" name="TradeId" value={defaultTradeId} />
          <Container>
            <Row>
              <Col xs="11">
                <FloatingLabel label="Портфель" className="mb-3">
                  <Form.Select aria-label="Выберите портфель" placeholder="Портфель" name="PortfolioId" defaultValue={defaultPortfolioId} required>
                    <option disabled>Выберите портфель</option>
                    {portfolios.map((obj, index) => {
                      return (
                        <option key={index} value={obj.portfolioId}>
                          {obj.name}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
              <Col xs="1" className="d-flex justify-content-center align-items-center">
                <Link to={"/portfolio/create"} className="mb-3">
                  <FontAwesomeIcon size="lg" icon={faPlus} />
                </Link>
              </Col>
            </Row>
            <Row>
              <Col>
                <FloatingLabel label="Класс актива" className="mb-3">
                  <Form.Select aria-label="Выберите класс актива" placeholder="Класс актива" name="AssetCategoryId" defaultValue={defaultAssetCategoryId} required>
                    <option disabled>Выберите портфель</option>
                    {assetCategories.map((obj, index) => {
                      return (
                        <option key={index} value={obj.assetCategoryId}>
                          {obj.name}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col>
                <FloatingLabel label="Валюта" className="mb-3">
                  <Form.Select aria-label="Выберите валюту" placeholder="Валюта" name="CurrencyId" defaultValue={defaultCurrencyId} required>
                    <option disabled>Выберите портфель</option>
                    {currencies.map((obj, index) => {
                      return (
                        <option key={index} value={obj.currencyId}>
                          {obj.symbol}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel label="Символ" className="mb-3">
                  <Form.Select aria-label="Выберите символ" placeholder="Символ" name="SymbolId" defaultValue={defaultSymbolId} required>
                    <option disabled>Выберите портфель</option>
                    {symbols.map((obj, index) => {
                      return (
                        <option key={index} value={obj.symbolId}>
                          {obj.value}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col>
                <FloatingLabel label="Дата/Время" className="mb-3">
                  <Form.Control type="date" placeholder="Дата/Время" name="Dt" defaultValue={defaultDt} required />
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel label="Статус" className="mb-3">
                  <Form.Select aria-label="Выберите статус" placeholder="Статус" name="StatusId" defaultValue={defaultStatusId} required>
                    <option disabled>Выберите портфель</option>
                    {statuses.map((obj, index) => {
                      return (
                        <option key={index} value={obj.statusId}>
                          {obj.name}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col>
                <FloatingLabel label="Цена транзакции" className="mb-3">
                  <Form.Control type="number" step={0.01} placeholder="Цена транзакции" name="PriceTransaction" defaultValue={defaultPriceTransaction} required />
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel label="Цена закрытия" className="mb-3">
                  <Form.Control type="number" step={0.01} placeholder="Цена закрытия" name="PriceClose" defaultValue={defaultPriceClose} required />
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col>
                <FloatingLabel label="Выручка" className="mb-3">
                  <Form.Control type="number" step={0.01} placeholder="Выручка" name="Proceed" defaultValue={defaultProceed} required />
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel label="Комиссия" className="mb-3">
                  <Form.Control type="number" step={0.01} placeholder="Комиссия" name="Commission" defaultValue={defaultCommission} required />
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col>
                <FloatingLabel label="Базис" className="mb-3">
                  <Form.Control type="number" step={0.01} placeholder="Базис" name="Basis" defaultValue={defaultBasis} required />
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel label="Реализованная П/У" className="mb-3">
                  <Form.Control type="number" step={0.01} placeholder="Реализованная П/У" name="RealizedPL" defaultValue={defaultRealizedPL} required />
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col>
                <FloatingLabel label="Рыноч. переоценка П/У" className="mb-3">
                  <Form.Control type="number" step={0.01} placeholder="Рыноч. переоценка П/У" name="MtmPL" defaultValue={defaultMtmPL} required />
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel label="Количество" className="mb-3">
                  <Form.Control type="number" placeholder="Количество" name="Count" defaultValue={defaultCount} required />
                </FloatingLabel>
              </Col>
            </Row>
          </Container>
        </Form>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {}
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    //
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TradeForm);
