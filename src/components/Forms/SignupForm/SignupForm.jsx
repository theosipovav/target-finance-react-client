import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "./SignupForm.module.scss";
import { Link } from "react-router-dom";
import iconSignup from "../../../img/signup.png";
import CardMessage from "../../UI/CardMessage/CardMessage";
import { ControlValidate, EmailForm, PasswordForm, TextForm } from "../../UI/ControlsForm/ControlsForm";
import { Alert } from "react-bootstrap";

class SignupForm extends Component {
  state = {
    status: null,
    message: null,
    isValidForm: false,
    formControls: {
      login: {
        name: "Login",
        value: "",
        label: "Логин",
        textHelp: "Введите логин для входа в систему",
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 3,
        },
      },
      password: {
        name: "Password",
        value: "",
        label: "Пароль",
        textHelp: "Введите пароль для входа в систему",
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6,
        },
      },
      passwordRepeat: {
        name: "PasswordRepeat",
        value: "",
        label: "Повторите пароль",
        textHelp: "Повторите введенный ранее пароль",
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6,
          match: "password",
        },
      },
      email: {
        name: "Email",
        value: "",
        label: "Email",
        textHelp: "Адрес электронной почты",
        valid: false,
        touched: false,
        validation: {
          required: true,
          email: true,
        },
      },
      fullName: {
        name: "Name",
        value: "",
        label: "ФИО",
        textHelp: "Введите ваше полное имя",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
    },
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отслеживание изменений ввода в элемент формы
   */
  onChangeHandler = (event, controlName) => {
    // Получение данных о элементах ввода формах
    const controls = { ...this.state.formControls };
    const control = { ...controls[controlName] };
    control.value = event.target.value;
    control.touched = true;
    control.valid = ControlValidate(event.target, control.validation);
    controls[controlName] = control;
    let isFormValid = true;
    Object.keys(controls).forEach((controlName) => {
      isFormValid = controls[controlName].valid && isFormValid;
    });
    this.setState({
      isFormValid,
      formControls: controls,
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <React.Fragment>
        <form className={styles.signupForm} onSubmit={(e) => this.props.handleSubmit(e)}>
          {!!this.state.status ? <CardMessage message={this.state.message} status={this.state.status}></CardMessage> : null}
          <div className="d-flex flex-column justify-content-center align-items-center">
            <img src={iconSignup} alt={"logo"} className={styles.signupIcon} />
            <h2 className="h2">
              <span className="me-2">
                <b>Регистрация</b>
              </span>
            </h2>
          </div>

          <div className={styles.signupFormBlock}>
            <TextForm
              name={this.state.formControls.login.name}
              label={this.state.formControls.login.label}
              textHelp={this.state.formControls.login.textHelp}
              textError={this.state.formControls.login.textError}
              valid={this.state.formControls.login.valid}
              touched={this.state.formControls.login.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "login");
              }}
            />
            <PasswordForm
              name={this.state.formControls.password.name}
              label={this.state.formControls.password.label}
              textHelp={this.state.formControls.password.textHelp}
              textError={this.state.formControls.password.textError}
              valid={this.state.formControls.password.valid}
              touched={this.state.formControls.password.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "password");
              }}
            />
            <PasswordForm
              name={this.state.formControls.passwordRepeat.name}
              label={this.state.formControls.passwordRepeat.label}
              textHelp={this.state.formControls.passwordRepeat.textHelp}
              textError={this.state.formControls.passwordRepeat.textError}
              valid={this.state.formControls.passwordRepeat.valid}
              touched={this.state.formControls.passwordRepeat.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "passwordRepeat");
              }}
            />

            <EmailForm
              name={this.state.formControls.email.name}
              label={this.state.formControls.email.label}
              textHelp={this.state.formControls.email.textHelp}
              textError={this.state.formControls.email.textError}
              valid={this.state.formControls.email.valid}
              touched={this.state.formControls.email.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "email");
              }}
            />
            <TextForm
              name={this.state.formControls.fullName.name}
              label={this.state.formControls.fullName.label}
              textHelp={this.state.formControls.fullName.textHelp}
              textError={this.state.formControls.fullName.textError}
              valid={this.state.formControls.fullName.valid}
              touched={this.state.formControls.fullName.touched}
              shouldValidate={true}
              onChange={(event) => {
                this.onChangeHandler(event, "fullName");
              }}
            />
          </div>
          {this.props.isFirstUser ? (
            <Alert variant="warning">
              <p>
                <b>Внимание !</b>
                <br />
                В системе не зарегистрировано ни одно пользователя.
                <br />
                Первому зарегистрировавшиеся пользователю присваивается роль "Администратор".
              </p>
            </Alert>
          ) : null}

          <div className="row">
            <div className="col-12 d-flex justify-content-center align-items-center">
              <div className="d-flex flex-column mt-4 mb-3">
                <button type="submit" className="btn btn-primary" style={{ width: "175px" }} disabled={!this.state.isFormValid}>
                  Зарегистрироваться
                </button>
                <Link to={"/signin"} className="btn btn-outline-primary color-primary mt-1" style={{ width: "175px" }}>
                  Войти
                </Link>
              </div>
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}
/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    user: JSON.parse(state.auth.userJson),
    role: JSON.parse(state.auth.roleJson),
    token: state.auth.token,
  };
}

export default connect(mapStateToProps, null)(SignupForm);
