import React, { Component } from "react";
import styles from "./TradeForm.module.scss";
import { connect } from "react-redux";
import { Form, Container, Row, Col, FloatingLabel, Button } from "react-bootstrap";
import PortfolioHelper from "../../../helpers/PortfolioHelper";
import PropTypes from "prop-types";
import Loader from "../../UI/Loader/Loader";
import { MessageSet } from "../../../store/actions/MessageAction";
import TradeHelper from "../../../helpers/TradeHelper";
import AssetCategoryHelper from "../../../helpers/AssetCategoryHelper";
import CurrencyHelper from "../../../helpers/CurrencyHelper";
import StatusHelper from "../../../helpers/StatusHelper";
import SymbolHelper from "../../../helpers/SymbolHelper";
import { Link, Navigate } from "react-router-dom";

/**
 * id
 *
 * Режимы (mode): readonly, edit, create
 */
class TradeForm extends Component {
  idForm = null;

  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.idForm = `${"Id"}${Math.random()}`;

    this.state = {
      loadingStep: 0,
      loadingMax: 6,
      portfolios: [],
      assetCategories: [],
      currencies: [],
      statuses: [],
      symbols: [],
      trade: {},
      mode: !!props.mode ? props.mode : "create",
      redirect: false,
    };
  }

  /**
   * Загрузка данных о сделки
   * @param {*} id Идентификатор сделки
   */
  loadTrade(id) {
    if (this.state.mode == "create" || !id) {
      let trade = {
        portfolio: {},
        assetCategory: {},
        currency: {},
        symbol: {},
        status: {},
        tradeId: null,
        dt: null,
        count: null,
        priceTransaction: null,
        priceClose: null,
        proceed: null,
        commission: null,
        basis: null,
        realizedPL: null,
        mtmPL: null,
      };
      this.setState({
        loadingStep: 1,
        trade: trade,
      });
    } else {
      this.setState({
        loadingStep: 0,
      });

      TradeHelper.GetOneById(id)
        .then((res) => {
          if (res.success) {
            let step = this.state.loadingStep + 1;
            let trade = res.data;
            let dt = new Date(trade.dt);
            trade.dt = dt.toISOString().substring(0, 10);
            this.setState({
              loadingStep: step,
              trade: trade,
            });
          } else this.props.MessageSet(null, res.data, null);
        })
        .catch((error) => {
          console.log(error);
          this.props.MessageSet(null, null, null);
          this.setState({
            loadingStep: 0,
          });
        });
    }

    PortfolioHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            portfolios: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    AssetCategoryHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            assetCategories: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    CurrencyHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            currencies: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    StatusHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            statuses: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });

    SymbolHelper.GetAll()
      .then((res) => {
        if (res.success) {
          let step = this.state.loadingStep + 1;
          this.setState({
            loadingStep: step,
            symbols: res.data,
          });
        } else this.props.MessageSet(null, res.data, null);
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
        this.setState({
          loadingStep: 0,
        });
      });
  }

  setMode = (mode) => {
    this.setState({ mode: mode });
  };

  handleCancel = () => {
    this.setMode("readonly");
    this.load(this.state.trade.tradeId);
  };

  handleSubmit = (e) => {
    this.setMode({
      loadingStep: 0,
    });
    e.preventDefault();
    if (this.state.mode == "edit") this.saveTrade(e.target);
    if (this.state.mode == "create") this.createTrade(e.target);
  };

  saveTrade = (form) => {
    this.setMode("readonly");
    const formData = new FormData(form);
    const data = {
      TradeId: -1,
      PortfolioId: formData.get("PortfolioId"),
      AssetCategoryId: formData.get("AssetCategoryId"),
      CurrencyId: formData.get("CurrencyId"),
      SymbolId: formData.get("SymbolId"),
      Dt: formData.get("Dt"),
      Count: formData.get("Count"),
      PriceTransaction: formData.get("PriceTransaction"),
      PriceClose: formData.get("PriceClose"),
      Proceed: formData.get("Proceed"),
      Commission: formData.get("Commission"),
      Basis: formData.get("Basis"),
      RealizedPL: formData.get("RealizedPL"),
      MtmPL: formData.get("MtmPL"),
      StatusId: formData.get("StatusId"),
    };

    TradeHelper.Update(data)
      .then((res) => {
        if (res.success) {
          this.loadTrade(this.state.trade.tradeId);
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        this.props.MessageSet("Пожалуйста, повторите попытку позже", "Ошибка при обращение к серверу", null);
        return false;
      });
  };

  createTrade = (form) => {
    const formData = new FormData(form);
    const data = {
      TradeId: 0,
      PortfolioId: formData.get("PortfolioId"),
      AssetCategoryId: formData.get("AssetCategoryId"),
      CurrencyId: formData.get("CurrencyId"),
      SymbolId: formData.get("SymbolId"),
      Dt: formData.get("Dt"),
      Count: formData.get("Count"),
      PriceTransaction: formData.get("PriceTransaction"),
      PriceClose: formData.get("PriceClose"),
      Proceed: formData.get("Proceed"),
      Commission: formData.get("Commission"),
      Basis: formData.get("Basis"),
      RealizedPL: formData.get("RealizedPL"),
      MtmPL: formData.get("MtmPL"),
      StatusId: formData.get("StatusId"),
    };

    const data2 = {
      TradeId: 0,
      PortfolioId: 0,
      AssetCategoryId: 0,
      CurrencyId: 0,
      SymbolId: 0,
      Dt: "2022-01-21T09:16:40.343Z",
      Count: 0,
      PriceTransaction: 0,
      PriceClose: 0,
      Proceed: 0,
      Commission: 0,
      Basis: 0,
      RealizedPL: 0,
      MtmPL: 0,
      StatusId: 0,
    };

    console.log(data);
    console.log(data2);

    TradeHelper.Create(data2)
      .then((res) => {
        if (res.success) {
          var trade = res.data;
          this.setState({
            redirect: "/trade/" + trade.tradeId,
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        this.props.MessageSet("Пожалуйста, повторите попытку позже", "Ошибка при обращение к серверу", null);
        return false;
      });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    if (!!this.state.redirect) return <Navigate to={this.state.redirect} />;
    if (this.state.loadingStep < this.state.loadingMax) return <Loader />;
    return (
      <React.Fragment>
        <Form id={this.idForm} onSubmit={(e) => this.handleSubmit(e)}>
          <Container>
            <Row>
              <Col>
                <div className="d-flex justify-content-end align-items-center my-3">
                  {this.state.mode == "readonly" ? <this.renderButtonForReadonly /> : null}
                  {this.state.mode == "edit" ? <this.renderButtonForEdit /> : null}
                  {this.state.mode == "create" ? <this.renderButtonForCreate /> : null}
                </div>
              </Col>
            </Row>
            {this.state.mode == "readonly" ? <this.renderFormReadOnly /> : null}
            {this.state.mode == "edit" || this.state.mode == "create" ? <this.renderFormDataEditOrCreate /> : null}
          </Container>
        </Form>
      </React.Fragment>
    );
  }

  renderButtonForReadonly = () => {
    return (
      <React.Fragment>
        <Button variant="secondary" onClick={(e) => this.setMode("edit")}>
          Редактировать
        </Button>
      </React.Fragment>
    );
  };

  renderButtonForEdit = () => {
    return (
      <React.Fragment>
        <Button variant="secondary" className="me-1" onClick={(e) => this.handleCancel(true)}>
          Отмена
        </Button>
        <Button variant="success" type="submit" form={this.idForm}>
          Сохранить
        </Button>
      </React.Fragment>
    );
  };
  renderButtonForCreate = () => {
    return (
      <React.Fragment>
        <Link to={"/trades"} className="btn btn-warning me-1">
          Отмена
        </Link>
        <Button variant="success" type="submit" form={this.idForm}>
          Сохранить
        </Button>
      </React.Fragment>
    );
  };

  renderFormDataEditOrCreate = () => {
    var portfolios = this.state.portfolios;
    var assetCategories = this.state.assetCategories;
    var currencies = this.state.currencies;
    var symbols = this.state.symbols;
    var statuses = this.state.statuses;
    var trade = this.state.trade;

    var defaultPortfolioId = trade.portfolio.portfolioTypeId !== undefined ? trade.portfolio.portfolioTypeId : null;
    var defaultAssetCategoryId = trade.assetCategory.assetCategoryId !== undefined ? trade.assetCategory.assetCategoryId : null;
    var defaultCurrencyId = trade.currency.currencyId !== undefined ? trade.currency.currencyId : null;
    var defaultSymbolId = trade.symbol.symbolId !== undefined ? trade.symbol.symbolId : null;
    var defaultDt = trade.dt !== undefined ? trade.dt : null;
    var defaultCount = trade.count !== undefined ? trade.count : null;
    var defaultPriceTransaction = trade.priceTransaction !== undefined ? trade.priceTransaction : null;
    var defaultPriceClose = trade.priceClose !== undefined ? trade.priceClose : null;
    var defaultProceed = trade.proceed !== undefined ? trade.proceed : null;
    var defaultCommission = trade.commission !== undefined ? trade.commission : null;
    var defaultBasis = trade.basis !== undefined ? trade.basis : null;
    var defaultRealizedPL = trade.realizedPL !== undefined ? trade.realizedPL : null;
    var defaultMtmPL = trade.mtmPL !== undefined ? trade.mtmPL : null;
    var defaultStatusId = trade.status.statusId !== undefined ? trade.status.statusId : null;

    return (
      <React.Fragment>
        <Row>
          <Col>
            <FloatingLabel label="Портфель" className="mb-3">
              <Form.Select aria-label="Выберите портфель" placeholder="Портфель" name="PortfolioId" defaultValue={defaultPortfolioId} required>
                <option disabled>Выберите портфель</option>
                {portfolios.map((obj, index) => {
                  return (
                    <option key={index} value={obj.PortfolioId}>
                      {obj.name}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Класс актива" className="mb-3">
              <Form.Select aria-label="Выберите класс актива" placeholder="Класс актива" name="AssetCategoryId" defaultValue={defaultAssetCategoryId} required>
                <option disabled>Выберите портфель</option>
                {assetCategories.map((obj, index) => {
                  return (
                    <option key={index} value={obj.assetCategoryId}>
                      {obj.name}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Валюта" className="mb-3">
              <Form.Select aria-label="Выберите валюту" placeholder="Валюта" name="CurrencyId" defaultValue={defaultCurrencyId} required>
                <option disabled>Выберите портфель</option>
                {currencies.map((obj, index) => {
                  return (
                    <option key={index} value={obj.currencyId}>
                      {obj.symbol}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Символ" className="mb-3">
              <Form.Select aria-label="Выберите символ" placeholder="Символ" name="SymbolId" defaultValue={defaultSymbolId} required>
                <option disabled>Выберите портфель</option>
                {symbols.map((obj, index) => {
                  return (
                    <option key={index} value={obj.symbolId}>
                      {obj.value}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Дата/Время" className="mb-3">
              <Form.Control type="date" placeholder="Дата/Время" name="Dt" defaultValue={defaultDt} required />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Количество" className="mb-3">
              <Form.Control type="number" placeholder="Количество" name="Count" defaultValue={defaultCount} required />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Цена транзакции" className="mb-3">
              <Form.Control type="number" step={0.01} placeholder="Цена транзакции" name="PriceTransaction" defaultValue={defaultPriceTransaction} required />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Цена закрытия" className="mb-3">
              <Form.Control type="number" step={0.01} placeholder="Цена закрытия" name="PriceClose" defaultValue={defaultPriceClose} required />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Выручка" className="mb-3">
              <Form.Control type="number" step={0.01} placeholder="Выручка" name="Proceed" defaultValue={defaultProceed} required />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Комиссия" className="mb-3">
              <Form.Control type="number" step={0.01} placeholder="Комиссия" name="Commission" defaultValue={defaultCommission} required />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Базис" className="mb-3">
              <Form.Control type="number" step={0.01} placeholder="Базис" name="Basis" defaultValue={defaultBasis} required />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Реализованная П/У" className="mb-3">
              <Form.Control type="number" step={0.01} placeholder="Реализованная П/У" name="RealizedPL" defaultValue={defaultRealizedPL} required />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Рыноч. переоценка П/У" className="mb-3">
              <Form.Control type="number" step={0.01} placeholder="Рыноч. переоценка П/У" name="MtmPL" defaultValue={defaultMtmPL} required />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Статус" className="mb-3">
              <Form.Select aria-label="Выберите статус" placeholder="Статус" name="StatusId" defaultValue={defaultStatusId} required>
                <option disabled>Выберите портфель</option>
                {statuses.map((obj, index) => {
                  return (
                    <option key={index} value={obj.statusId}>
                      {obj.name}
                    </option>
                  );
                })}
              </Form.Select>
            </FloatingLabel>
          </Col>
        </Row>
      </React.Fragment>
    );
  };

  renderFormReadOnly = () => {
    var trade = this.state.trade;
    return (
      <React.Fragment>
        <Row>
          <Col>
            <FloatingLabel label="Портфель" className="mb-3">
              <Form.Control type="text" placeholder="Портфель" defaultValue={trade.portfolio.name} readOnly />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Класс актива" className="mb-3">
              <Form.Control type="text" placeholder="Класс актива" defaultValue={trade.assetCategory.name} readOnly />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Валюта" className="mb-3">
              <Form.Control type="text" placeholder="Валюта" defaultValue={trade.currency.name} readOnly />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Символ" className="mb-3">
              <Form.Control type="text" placeholder="Символ" defaultValue={trade.symbol.value} readOnly />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Дата/Время" className="mb-3">
              <Form.Control type="text" placeholder="Дата/Время" defaultValue={trade.dt} readOnly />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Количество" className="mb-3">
              <Form.Control type="text" placeholder="Количество" defaultValue={trade.count} readOnly />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Цена транзакции" className="mb-3">
              <Form.Control type="text" placeholder="Цена транзакции" defaultValue={trade.priceTransaction} readOnly />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Цена закрытия" className="mb-3">
              <Form.Control type="text" placeholder="Цена закрытия" defaultValue={trade.priceClose} readOnly />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Выручка" className="mb-3">
              <Form.Control type="text" placeholder="Выручка" defaultValue={trade.proceed} readOnly />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Комиссия" className="mb-3">
              <Form.Control type="text" placeholder="Комиссия" defaultValue={trade.commission} readOnly />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Базис" className="mb-3">
              <Form.Control type="text" placeholder="Базис" defaultValue={trade.basis} readOnly />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Реализованная П/У" className="mb-3">
              <Form.Control type="text" placeholder="Реализованная П/У" defaultValue={trade.realizedPL} readOnly />
            </FloatingLabel>
          </Col>
        </Row>
        <Row>
          <Col>
            <FloatingLabel label="Рыноч. переоценка П/У" className="mb-3">
              <Form.Control type="text" placeholder="Рыноч. переоценка П/У" defaultValue={trade.mtmPL} readOnly />
            </FloatingLabel>
          </Col>
          <Col>
            <FloatingLabel label="Статус" className="mb-3">
              <Form.Control type="text" placeholder="Статус" defaultValue={trade.status.name} readOnly />
            </FloatingLabel>
          </Col>
        </Row>
      </React.Fragment>
    );
  };

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    if (this.state.mode == "create") {
      this.loadTrade(false);
    } else {
      this.loadTrade(this.props.id);
    }
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    //
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TradeForm);
