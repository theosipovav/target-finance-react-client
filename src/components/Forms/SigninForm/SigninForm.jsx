import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "./SigninForm.module.scss";
import StylesForm from "../Forms.module.scss";
import { Link } from "react-router-dom";
import iconSignin from "../../../img/signin.png";
import { ActionLogin } from "../../../store/actions/ActionLogin";
import { SubmitForm } from "../../UI/SubmitForm/SubmitForm";
import { ControlValidate, PasswordForm, TextForm } from "../../UI/ControlsForm/ControlsForm";
import CardMessage from "../../UI/CardMessage/CardMessage";

class SigninForm extends Component {
  state = {
    isFormLoading: false,
    isValidForm: false,
    formControls: {
      login: {
        value: "",
        label: "Логин",
        textHelp: "Введите логин для входа в систему",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      password: {
        value: "",
        label: "Пароль",
        textHelp: "Введите пароль для входа в систему",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
    },
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отслеживание изменений ввода в элемент формы
   */
  onChangeHandler = (event, controlName) => {
    // Получение данных о элементах ввода формах
    const formControls = { ...this.state.formControls };
    const control = { ...formControls[controlName] };
    //
    // Проверка валидации
    control.value = event.target.value;
    control.touched = true;
    control.valid = ControlValidate(event.target, control.validation);
    formControls[controlName] = control;

    let isFormValid = true;
    Object.keys(formControls).forEach((controlName) => {
      isFormValid = formControls[controlName].valid && isFormValid;
    });

    // Сохранение в state
    this.setState({
      isFormValid,
      formControls,
    });
  };

  /**
   * Отправка формы
   * @param {*} e
   */
  async handleSubmit(e) {
    e.preventDefault();
    this.setState({
      isFormLoading: true,
    });
    this.props.login(this.state.formControls.login.value, this.state.formControls.password.value);
    this.setState({
      isFormLoading: false,
    });
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    let clsForm = [styles.signupForm];
    if (this.state.isFormLoading) {
      clsForm.push(StylesForm.FormLoading);
    }

    return (
      <form
        className={clsForm.join(" ")}
        onSubmit={(e) => {
          this.handleSubmit(e);
        }}
      >
        {this.props.authError ? <CardMessage status="error" message="Неверный логин или пароль" /> : null}
        <div className="d-flex flex-column justify-content-center align-items-center">
          <img src={iconSignin} alt={"logo"} className={styles.signupIcon} />
          <h2 className="h2">
            <span className="me-2">
              <b>Авторизация</b>
            </span>
          </h2>
        </div>
        <div className={styles.signupFormBlock}>
          <TextForm
            label={this.state.formControls.login.label}
            name={this.state.formControls.login.name}
            textHelp={this.state.formControls.login.textHelp}
            valid={this.state.formControls.login.valid}
            touched={this.state.formControls.login.touched}
            shouldValidate={this.state.formControls.login.shouldValidate}
            onChange={(event) => {
              this.onChangeHandler(event, "login");
            }}
          />
          <PasswordForm
            label={this.state.formControls.password.label}
            name={this.state.formControls.password.name}
            textHelp={this.state.formControls.password.textHelp}
            valid={this.state.formControls.password.valid}
            touched={this.state.formControls.password.touched}
            shouldValidate={this.state.formControls.password.shouldValidate}
            onChange={(event) => {
              this.onChangeHandler(event, "password");
            }}
          />
        </div>
        <div className="row">
          <div className="col-12 d-flex justify-content-center align-items-center">
            <div className="d-flex flex-column mt-4 mb-3">
              <SubmitForm className={"btn btn-primary"} disabled={!this.state.isFormValid}>
                Войти
              </SubmitForm>
              <Link to={"/signup"} className="btn btn-outline-primary color-primary mt-1" style={{ width: "175px" }}>
                Регистрация
              </Link>
            </div>
          </div>
        </div>
      </form>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

/**
 * Получает state из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.user,
    authError: !!state.auth.authError,
  };
}

/**
 * Отправляет данные в Redux
 * Redux
 * @param {*} dispatch
 * @returns Новый state
 */
function mapDispatchToProps(dispatch) {
  return {
    login: (email, password) => dispatch(ActionLogin(email, password)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SigninForm);
