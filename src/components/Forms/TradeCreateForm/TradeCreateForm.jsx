import React, { Component } from "react";
import { connect } from "react-redux";
import { GET_PORTFOLIOS_USER, GET_ASSET_CATEGORY_ALL, GET_CURRENCY_ALL, GET_STATUS_ALL, GET_SYMBOL_ALL } from "../../../rest/urls";

class TradeCreateForm extends Component {
  //
  state = {
    loading: true,
    loadingStep: 0,
    loadingMaxStep: 0,
    status: null,
    message: null,
    isFormLoading: false,
    isFormValid: false,

    portfolios: [],
    assetCategories: [],
    currencies: [],
    symbols: [],
    statuses: [],

    formControls: {
      Portfolio: {
        name: "PortfolioId",
        label: "Портфель",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      AssetCategory: {
        name: "AssetCategoryId",
        label: "Класс актива",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Currency: {
        name: "CurrencyId",
        label: "Валюта",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Symbol: {
        name: "SymbolId",
        label: "символ",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Dt: {
        name: "Dt",
        label: "Дата/Время",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Count: {
        name: "Count",
        label: "Количество",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      PriceTransaction: {
        name: "PriceTransaction",
        label: "Цена транзакции",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      PriceClose: {
        name: "PriceClose",
        label: "Цена закрытия",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Proceed: {
        name: "Proceed",
        label: "Выручка",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Commission: {
        name: "Commission",
        label: "Комиссия",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Basis: {
        name: "Basis",
        label: "Базис",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      RealizedPL: {
        name: "RealizedPL",
        label: "Реализованная П/У",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      MtmPL: {
        name: "MtmPL",
        label: "Рыноч. переоценка П/У",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
      Status: {
        name: "StatusId",
        label: "Статус",
        valid: false,
        touched: false,
        validation: {
          required: true,
        },
      },
    },
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Выполняется до отрисовки компонента
   */
  componentWillMount() {
    //
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <React.Fragment>
        <form>
          {!!this.state.status ? <CardMessage message={this.state.message} status={this.state.status}></CardMessage> : null}

          <div className={styles.signupFormBlock}></div>
        </form>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    const headers = {
      Accept: "application/json",
      Authorization: "Bearer " + this.props.token,
    };

    // Загрузка портфелей
    axios
      .get(GET_PORTFOLIOS_USER, { headers: headers })
      .then((response) => {
        let portfolios = response.data;
        let loadingStep = this.state.loadingStep + 1;
        this.setState({
          portfolios: portfolios,
          loadingStep: loadingStep,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          status: "error",
        });
      });

    // Загрузка классов актива
    axios
      .get(GET_ASSET_CATEGORY_ALL, { headers: headers })
      .then((response) => {
        let assetCategories = response.data;
        let loadingStep = this.state.loadingStep + 1;
        this.setState({
          assetCategories: assetCategories,
          loadingStep: loadingStep,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          status: "error",
        });
      });

    // Загрузка валют
    axios
      .get(GET_CURRENCY_ALL, { headers: headers })
      .then((response) => {
        let currencies = response.data;
        let loadingStep = this.state.loadingStep + 1;
        this.setState({
          currencies: currencies,
          loadingStep: loadingStep,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          status: "error",
        });
      });

    // Загрузка статусов
    axios
      .get(GET_STATUS_ALL, { headers: headers })
      .then((response) => {
        let statuses = response.data;
        let loadingStep = this.state.loadingStep + 1;
        this.setState({
          statuses: statuses,
          loadingStep: loadingStep,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          status: "error",
        });
      });

    // Загрузка статусов
    axios
      .get(GET_SYMBOL_ALL, { headers: headers })
      .then((response) => {
        let symbols = response.data;
        let loadingStep = this.state.loadingStep + 1;
        this.setState({
          symbols: symbols,
          loadingStep: loadingStep,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          status: "error",
        });
      });
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    user: JSON.parse(state.auth.userJson),
    role: JSON.parse(state.auth.roleJson),
    token: state.auth.token,
  };
}

export default connect(mapStateToProps, null)(TradeCreateForm);
