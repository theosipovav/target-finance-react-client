import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import HelpBlock from "../../UI/HelpBlock/HelpBlock";
import InputFile from "../../UI/InputFile/InputFile";
import { GET_PORTFOLIOS_USER } from "../../../rest/urls";
import Loader from "../../UI/Loader/Loader";
import stylesMain from "../../../index.module.scss";
import CardMessage from "../../UI/CardMessage/CardMessage";
import ImportHelper from "../../../helpers/ImportHelper";
import LogHelper from "../../../helpers/LogHelper";
import PortfolioHelper from "../../../helpers/PortfolioHelper";
import { MessageSet } from "../../../store/actions/MessageAction";
import { Button, Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

class ImportForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingStep: 0,
      loadingMax: 2,
      status: null,
      message: null,
      selectedFile: null,
      selectedPortfolioId: -1,
      portfolioList: [],
      logs: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      selectedFile: event.target.files[0],
    });
  }

  handleChangeSelect(e) {
    this.setState({
      selectedPortfolioId: e.target.value,
    });
  }

  /**
   * Отправка формы
   * @param {*} event
   */
  async handleSubmit(e) {
    e.preventDefault();

    this.setState({
      loadingStep: 0,
    });
    let form = e.target;
    const formData = new FormData(form);
    ImportHelper.load(formData)
      .then((res) => {
        if (res.success) {
          console.log(res);
          this.setState({
            status: "success",
            message: "Импорт выполнен успешно",
          });
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, null, null);
      })
      .then((e) => {
        this.load();
      });
  }

  handleRemoveLog = (id) => {
    LogHelper.Remove(id)
      .then((res) => {
        if (res.success) {
          this.load();
        } else {
          this.props.MessageSet(null, res.data, null);
        }
      })
      .catch((error) => {
        this.props.MessageSet(null, null, null);
      });
  };

  render() {
    const clsButton = ["btn", "btn-primary", "btn-lg", "px-5"];
    const isSelectedFile = !!this.state.selectedFile;
    const isSelectedPortfolio = this.state.selectedPortfolioId == -1 ? false : true;
    if (!isSelectedFile || !isSelectedPortfolio) {
      clsButton.push("disabled");
    }
    let portfolioList = this.state.portfolioList;
    let idSelect = "id" + Math.random().toString(16).slice(2);

    if (this.state.loading) {
      return <Loader />;
    }

    return (
      <React.Fragment>
        {!!this.state.status ? <CardMessage message={this.state.message} status={this.state.status}></CardMessage> : null}
        <form onSubmit={this.handleSubmit}>
          <div className="d-flex flex-column">
            <div className="d-flex justify-content-center align-items-center mb-2">
              <label htmlFor={idSelect} className="me-2">
                Выберите сервис, откуда был произведен экспорт сделок:
              </label>
              <div className="d-flex flex-grow-1">
                <select className="form-select" required defaultValue={1}>
                  <option disabled value={-1}>
                    Выберите портфель
                  </option>
                  <option disabled value={1}>
                    Interactive Brokers
                  </option>
                </select>
              </div>
            </div>

            <div className="d-flex justify-content-center align-items-center mb-1">
              <label htmlFor={idSelect} className="me-2">
                Портфель:
              </label>
              <div className="d-flex flex-grow-1">
                <select id={idSelect} name="PortfolioId" className="form-select" onChange={(e) => this.handleChangeSelect(e)} required defaultValue={-1}>
                  <option disabled value={-1}>
                    Выберите портфель
                  </option>
                  {portfolioList.map((p, index) => {
                    return (
                      <option key={index} value={p.portfolioId}>
                        {p.name}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>
            <div className="d-flex my-3">
              <InputFile name={"formFile"} onFileChange={(event) => this.handleChange(event)} />
            </div>
            <HelpBlock>
              Допустимые расширения файлов: <b>CSV</b>
            </HelpBlock>
            <div className="d-flex justify-content-center align-content-center my-3">
              <button type="submit" className={clsButton.join(" ")}>
                Загрузить
              </button>
            </div>
          </div>
        </form>
        <div className="row">
          <div className="col-12">
            <h3 className="h4 mt-3">Выполненные импорты</h3>
          </div>
          <div className="col-12">
            <this.renderTableLog />
          </div>
        </div>
      </React.Fragment>
    );
  }

  renderTableLog = () => {
    var logs = this.state.logs;
    return (
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>ID</th>
            <th>Дата выполнения импорта</th>
            <th>HASH файла</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.state.logs.map((obj, index) => {
            var dt = new Date(obj.dt);
            var dtString = dt.toLocaleDateString("ru-Ru");
            return (
              <tr key={index}>
                <td>{obj.logImportId}</td>
                <td>{dtString}</td>
                <td>{obj.hashFile}</td>
                <td className="text-center">
                  <Button variant={"link"} className="text-danger" onClick={() => this.handleRemoveLog(obj.logImportId)}>
                    <FontAwesomeIcon icon={faTrash}></FontAwesomeIcon>
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    );
  };

  load = () => {
    PortfolioHelper.GetAll()
      .then((response) => {
        if (response.success) {
          var step = this.loadingStep + 1;
          this.setState({
            loadingStep: step,
            portfolioList: response.data,
          });
        } else {
          this.props.MessageSet(null, response.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response.data, null);
      });

    LogHelper.GetAll()
      .then((response) => {
        if (response.success) {
          var step = this.loadingStep + 1;
          this.setState({
            loadingStep: step,
            logs: response.data,
          });
        } else {
          this.props.MessageSet(null, response.data, null);
        }
      })
      .catch((error) => {
        console.log(error);
        this.props.MessageSet(null, error.response.data, null);
      });
  };

  /**
   * componentDidMount
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    this.load();
  }
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  return {
    user: JSON.parse(state.auth.userJson),
    role: JSON.parse(state.auth.roleJson),
    token: state.auth.token,
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    MessageSet: (text, title, header) => dispatch(MessageSet(true, text, title, header)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ImportForm);
