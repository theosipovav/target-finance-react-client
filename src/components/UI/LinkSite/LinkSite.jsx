import React from "react";
import { Link } from "react-router-dom";

const LinkSite = (props) => {
  let strClass = "btn";
  if (!!this.props.className) {
    strClass += " " + this.props.className;
  } else {
    strClass += " btn-primary";
  }

  return (
    <Link to={this.props.to} className={strClass}>
      {props.text}
    </Link>
  );
};

export default LinkSite;
