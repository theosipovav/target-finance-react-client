import React from "react";

const CheckboxForm = (props) => {
  let label = props.label;
  let id = props.id;
  let name = props.name;
  /*
  let textHelp = props.textHelp;
  let idTextHelp = idInput + "Help";
    */
  return (
    <div>
      <div className="row">
        <div className="col-12 d-flex justify-content-center align-items-center">
          <div className="d-flex mt-2">
            <input type="checkbox" className="form-check-input" id={id} name={name} />
            <label className="form-check-label ms-2" htmlFor={id}>
              {label}
            </label>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CheckboxForm;
