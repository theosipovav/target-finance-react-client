import React from "react";

export const ButtonForm = (props) => {
  return (
    <button type="button" onClick={props.onClick} className={props.className} disabled={props.disabled}>
      {props.children}
    </button>
  );
};
