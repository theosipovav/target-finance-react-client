import React, { Component } from "react";
import styles from "./InputFile.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileImport } from "@fortawesome/free-solid-svg-icons";

class InputFile extends Component {
  state = {
    file: null,
  };

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  onChange(event) {
    const file = event.target.files[0];
    this.setState({
      file: file,
    });
    this.props.onFileChange(event);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    let name = "file";
    if (!!this.props.name) {
      name = this.props.name;
    }

    let desc = 'Перетащите или нажмите "Выберите файл"';
    if (!!this.state.file) {
      desc = this.state.file.name;
    }

    return (
      <label className={styles.InputFileLabel}>
        <div className="d-flex justify-content-center align-content-center mt-1 mb-1">
          <FontAwesomeIcon icon={faFileImport} className={styles.InputFileIcon} />
          <span>Загрузите отчет по сделкам</span>
        </div>
        <p className={styles.InputFileHelp}>{desc}</p>
        <input type="file" name={name} className={styles.InputFileHidden} onChange={(event) => this.onChange(event)} />
      </label>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}
export default InputFile;
