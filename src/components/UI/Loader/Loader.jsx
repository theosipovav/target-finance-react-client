import React from "react";
import styles from "./Loader.module.scss";

const Loader = (props) => {
  let cls = [styles.loader];
  if (!!props.white) {
    cls.push(styles.LoaderWhite);
  }

  return (
    <div className="d-flex justify-content-center align-items-center m-3">
      <div className={cls.join(" ")}>
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
};

export default Loader;
