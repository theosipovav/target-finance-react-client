import React from "react";
import style from "./ListSite.module.scss";

const ListSite = (props) => {
  return (
    <React.Fragment>
      {!!props.title ? <p>{props.title}</p> : null}
      <ul className={style.listSite}>{props.children}</ul>
    </React.Fragment>
  );
};

export default ListSite;
