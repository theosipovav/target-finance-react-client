import React from "react";
import styles from "./ControlsForm.module.scss";

function isInvalid({ valid, touched, shouldValidate }) {
  return !valid && shouldValidate && touched;
}

/**
 * Проверка валидации элемента формы
 * @param {*} element Элемент формы
 * @param {*} rules правила валидации
 * @returns
 */
export function ControlValidate(element, rules) {
  if (!rules) return true;
  let isValid = true;
  let text = "";

  let value = element.value;

  // Обязательное заполнение поля
  if (!!rules.required) {
    isValid = value.trim() !== "" && isValid;
  }
  // Значение поля является электронным адресом
  if (!!rules.email) {
    isValid =
      String(value)
        .toLowerCase()
        .match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) && isValid;
  }
  // Минимальная длина
  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;
  }
  return isValid;
}

export const TextForm = (props) => {
  let id = `${"Text"}${Math.random()}`;
  const classTextHelp = ["form-text", styles.formText];
  const classFormBlock = [styles.FormBlock];
  const clsControl = ["form-control"];
  if (isInvalid(props)) {
    clsControl.push("is-invalid");
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12 col-md-3">
          <label htmlFor={id} className="form-label">
            {props.label}
          </label>
        </div>
        <div className="col-12 col-md-9">
          <input type="text" className={clsControl.join(" ")} name={props.name} id={id} aria-describedby={id + "Help"} value={props.value} onChange={props.onChange} />
          {!!props.textHelp ? (
            <div id={id + "Help"} className={classTextHelp.join(" ")}>
              {props.textHelp}
            </div>
          ) : null}
          {isInvalid(props) ? <div className="text-danger mb-2">{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};

export const DateForm = (props) => {
  let id = `${"Date"}${Math.random()}`;
  const classTextHelp = ["form-text", styles.formText];
  const classFormBlock = [styles.FormBlock];
  const clsControl = ["form-control"];
  if (isInvalid(props)) {
    console.log("is-invalid");
    clsControl.push("is-invalid");
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12 col-md-3">
          <label htmlFor={id} className="form-label">
            {props.label}
          </label>
        </div>
        <div className="col-12 col-md-9">
          <input className={clsControl.join(" ")} type="date" name={props.name} id={id} aria-describedby={id + "Help"} onChange={props.onChange}></input>
          {!!props.textHelp ? (
            <div id={id + "Help"} className={classTextHelp.join(" ")}>
              {props.textHelp}
            </div>
          ) : null}

          {isInvalid(props) ? <div className="text-danger">{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};

export const TextareaForm = (props) => {
  let id = `${"Textarea"}${Math.random()}`;
  const classTextHelp = ["form-text", styles.formText];
  let classFormBlock = ["mt-3"];
  const clsControl = ["form-control"];
  if (isInvalid(props)) {
    console.log("is-invalid");
    clsControl.push("is-invalid");
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12">
          <div className="form-floating">
            <textarea className={clsControl.join(" ")} placeholder="..." id={id} onChange={props.onChange}></textarea>
            <label htmlFor="floatingTextarea2">{props.label}</label>
          </div>
          {!!props.textHelp ? (
            <div id={id + "Help"} className={classTextHelp.join(" ")}>
              {props.textHelp}
            </div>
          ) : null}
          {isInvalid(props) ? <div className="text-danger">{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};

export const NumberForm = (props) => {
  let typeInput = props.typeInput || "text";
  let id = `${typeInput}${Math.random()}`;
  const classTextHelp = ["form-text", styles.formText];
  const classFormBlock = [styles.FormBlock];
  let step = !!props.step ? props.step : 1;

  const clsControl = ["form-control"];
  if (isInvalid(props)) {
    console.log("is-invalid");
    clsControl.push("is-invalid");
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12 col-md-3">
          <label htmlFor={id} className="form-label">
            {props.label}
          </label>
        </div>
        <div className="col-12 col-md-9">
          <input type="number" step={step} className={clsControl.join(" ")} name={props.name} id={id} aria-describedby={id + "Help"} value={props.value} onChange={props.onChange} />
          {!!props.textHelp ? (
            <div id={id + "Help"} className={classTextHelp.join(" ")}>
              {props.textHelp}
            </div>
          ) : null}
          {isInvalid(props) ? <div className="text-danger">{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};

export const EmailForm = (props) => {
  let id = `${"Email"}${Math.random()}`;
  const classTextHelp = ["form-text", styles.formText];
  const classFormBlock = [styles.FormBlock];
  const clsControl = ["form-control"];
  if (isInvalid(props)) {
    console.log("is-invalid");
    clsControl.push("is-invalid");
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12 col-md-3">
          <label htmlFor={id} className="form-label">
            {props.label}
          </label>
        </div>
        <div className="col-12 col-md-9">
          <input type="email" className={clsControl.join(" ")} name={props.name} id={id} aria-describedby={id + "Help"} value={props.value} onChange={props.onChange} />
          {!!props.textHelp ? (
            <div id={id + "Help"} className={classTextHelp.join(" ")}>
              {props.textHelp}
            </div>
          ) : null}
          {isInvalid(props) ? <div className="text-danger">{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};

export const PasswordForm = (props) => {
  let id = `${"Password"}${Math.random()}`;
  const classTextHelp = ["form-text", styles.formText];
  const classFormBlock = [styles.FormBlock];
  const clsControl = ["form-control"];
  if (isInvalid(props)) {
    console.log("is-invalid");
    clsControl.push("is-invalid");
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12 col-md-3">
          <label htmlFor={id} className="form-label">
            {props.label}
          </label>
        </div>
        <div className="col-12 col-md-9">
          <input type="password" className={clsControl.join(" ")} name={props.name} id={id} aria-describedby={id + "Help"} value={props.value} onChange={props.onChange} />
          {!!props.textHelp ? (
            <div id={id + "Help"} className={classTextHelp.join(" ")}>
              {props.textHelp}
            </div>
          ) : null}
          {isInvalid(props) ? <div className="text-danger">{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};

export const SelectForm = (props) => {
  let id = `${"Select"}${Math.random()}`;

  let options = JSON.parse(props.options);

  const classTextHelp = ["form-text"];
  classTextHelp.push(styles.formText);

  const classFormBlock = [styles.FormBlock];
  const clsControl = ["form-select", styles.SelectForm];
  if (isInvalid(props)) {
    console.log("is-invalid");
    clsControl.push("is-invalid");
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12 col-md-3">
          <label htmlFor={id} className="form-label">
            {props.label}
          </label>
        </div>
        <div className="col-12 col-md-9">
          <select id={id} name={props.name} className={clsControl.join(" ")} rows="10" onChange={props.onChange} required defaultValue={-1}>
            <option disabled value={-1}>
              Выберите портфель
            </option>
            {options.map((obj, index) => {
              return (
                <option key={index} value={obj.value}>
                  {obj.text}
                </option>
              );
            })}
          </select>
          {!!props.textHelp ? (
            <div id={id + "Help"} className={classTextHelp.join(" ")}>
              {props.textHelp}
            </div>
          ) : null}
          {isInvalid(props) ? <div className="text-danger">{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};
