import React from "react";
import styles from "./HelpBlock.module.scss";

const Lead = (props) => {
  return <p className={styles.HelpBlock}>{props.children}</p>;
};

export default Lead;
