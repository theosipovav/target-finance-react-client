import React from "react";

const Jumbotron = (props) => {
  return (
    <React.Fragment>
      <div className="p-5 mb-4 rounded-3">
        <div className="container-fluid py-5">
          <h1 className="display-5 fw-bold">{props.title}</h1>
          <div className="col-md-8 fs-4">{props.children}</div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Jumbotron;
