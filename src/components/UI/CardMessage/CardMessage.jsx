import React, { Component } from "react";
import styles from "./CardMessage.module.scss";

/**
 * Карточка с сообщением
 * Параметры:
 * status = (success || warning || error)
 * header
 * title
 * message
 *
 */
class CardMessage extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    let message = this.props.message;
    let header = !!this.props.header ? this.props.header : "Внимание";
    let title = this.props.title;
    let clsCard = ["card", "mb-3"];
    switch (this.props.status) {
      case "success":
        clsCard.push("text-white");
        clsCard.push("bg-success");
        break;
      case "warning":
        clsCard.push("text-dark");
        clsCard.push("bg-warning");
        break;
      default:
        clsCard.push("text-white");
        clsCard.push("bg-danger");
        break;
    }
    return (
      <div className={clsCard.join(" ")}>
        <div className="card-header">{header}</div>
        <div className="card-body">
          {!!title ? <h5 className="card-title">{title}</h5> : null}
          <p className="card-text text-wrap">{message}</p>
        </div>
      </div>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}
export default CardMessage;
