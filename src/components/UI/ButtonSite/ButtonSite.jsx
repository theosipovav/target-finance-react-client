import React from "react";

const ButtonSite = (props) => {
  let strClass = "btn";
  if (!!this.props.className) {
    strClass += " " + this.props.className;
  } else {
    strClass += " btn-primary";
  }

  return (
    <button className={strClass} type="button">
      {props.text}
    </button>
  );
};

export default ButtonSite;
