import React from "react";
import styles from "./InputForm.module.scss";

function isInvalid({ valid, touched, shouldValidate }) {
  return !valid && shouldValidate && touched;
}

const InputForm = (props) => {
  let typeInput = props.typeInput || "text";
  let id = `${typeInput}${Math.random()}`;
  const classInput = ["form-control"];
  classInput.push(styles.formControl);
  const classTextHelp = ["form-text"];
  classTextHelp.push(styles.formText);
  const classFormBlock = [];
  if (isInvalid(props)) {
    classFormBlock.push(styles.FormControlError);
  }
  return (
    <div className={classFormBlock.join(" ")}>
      <div className="row">
        <div className="col-12 col-md-3">
          <label htmlFor={id} className="form-label">
            {props.label}
          </label>
        </div>
        <div className="col-12 col-md-9">
          <input type={typeInput} className={classInput.join(" ")} name={props.name} id={id} aria-describedby={id + "Help"} value={props.value} onChange={props.onChange} />
          <div id={id + "Help"} className={classTextHelp.join(" ")}>
            {props.textHelp}
          </div>
          {isInvalid(props) ? <div className={styles.formError}>{props.textError || "Введите верное значение"}</div> : null}
        </div>
      </div>
    </div>
  );
};

export default InputForm;
