import React from "react";

export const SubmitForm = (props) => {
  return (
    <button type="submit" className={props.className} disabled={props.disabled}>
      {props.children}
    </button>
  );
};
