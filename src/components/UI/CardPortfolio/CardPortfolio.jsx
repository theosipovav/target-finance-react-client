import React, { Component } from "react";
import styles from "./CardPortfolio.module.scss";
import { faInfoCircle, faTrash, faFolderOpen, faPlus, faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

/**
 * id - Идентификатор портфеля в БД
 * name - Наименование портфеля
 * type - Наименование типа портфеля
 * dt - Дата открытия
 * note - Заметка
 * onDelete - Событие на удаление
 */
class CardPortfolio extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    let clsCard = ["card", styles.PortfolioCard];
    return (
      <React.Fragment>
        <div className={clsCard.join(" ")}>
          <div className="card-body d-flex flex-column">
            <h5 className="card-title">{this.props.name}</h5>
            <div className="card-text d-flex flex-column flex-grow-1">
              <div className="d-flex">Тип: {this.props.type}</div>
              <div className="d-flex">Дата открытия: {this.props.dt}</div>
              <p className="mt-1">{this.props.note}</p>
            </div>
            <div className="d-flex justify-content-end mt-1">
              <div className="btn-group " role="group">
                <Link to={"/portfolio/" + this.props.id} className="btn btn-outline-primary">
                  <FontAwesomeIcon icon={faEye} />{" "}
                </Link>
                <button type="button" className="btn btn-outline-primary" title="Удалить" onClick={this.props.onDelete}>
                  <FontAwesomeIcon icon={faTrash} />
                </button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default CardPortfolio;
