import React, { Component } from "react";
import "./PortfolioSummary.Module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDoubleUp, faAngleDoubleDown } from "@fortawesome/free-solid-svg-icons";

export class PortfolioSummary extends Component {
  render() {
    const classWrapperCard = ["col-12", "col-sx-6", "col-md-6", "col-lg-3"];

    let price = 39218.4;
    let priceUSD = price / 75;
    price = price.toFixed(2);
    priceUSD = priceUSD.toFixed(2);

    let profit = 39118.4;
    let profitPercent = 39118.4;
    profit = profit.toFixed(2);
    const classValueProfit = ["card-text"];
    if (profitPercent > 0) {
      classValueProfit.push("dashboardValueSuccess");
    } else {
      classValueProfit.push("dashboardValueDanger");
    }

    let profitability = 39118.44;
    let changeDay = -140.4;
    let changeDayPercent = -0.36;
    const classChangeDay = ["card-text"];
    if (changeDayPercent > 0) {
      classChangeDay.push("dashboardValueSuccess");
    } else {
      classChangeDay.push("dashboardValueDanger");
    }

    const iconUp = <FontAwesomeIcon icon={faAngleDoubleUp} />;
    const iconDown = <FontAwesomeIcon icon={faAngleDoubleDown} />;

    return (
      <div className="dashboar-summary bg-primary">
        <div className="row">
          <div className={classWrapperCard.join(" ")}>
            <div className="card dashboar-summary-card">
              <div className="card-body bg-primary text-white">
                <p>Стоимость</p>
                <p className="card-text">{price} RUB</p>
                <p className="card-text">{priceUSD} USD</p>
              </div>
            </div>
          </div>
          <div className={classWrapperCard.join(" ")}>
            <div className="card dashboar-summary-card">
              <div className="card-body bg-primary text-white">
                <p>Прибыль</p>
                <p className="card-text">{profit} RUB</p>
                <p className={classValueProfit.join(" ")}>
                  <span className="me-2">{profitPercent > 0 ? iconUp : iconDown}</span>
                  <span className="me-2">{profitPercent}</span>
                  <span>%</span>
                </p>
              </div>
            </div>
          </div>
          <div className={classWrapperCard.join(" ")}>
            <div className="card dashboar-summary-card">
              <div className="card-body bg-primary text-white">
                <p>Доходность</p>
                <p className="card-text">{profitability} %</p>
              </div>
            </div>
          </div>
          <div className={classWrapperCard.join(" ")}>
            <div className="card dashboar-summary-card">
              <div className="card-body bg-primary text-white">
                <p>Изменение за день</p>
                <p className="card-text">{changeDay} RUB</p>
                <p className={classChangeDay.join(" ")}>
                  <span className="me-2">{changeDayPercent > 0 ? iconUp : iconDown}</span>
                  <span className="me-2">{changeDayPercent}</span>
                  <span>%</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PortfolioSummary;
