import React, { Component } from "react";

export class TableActive extends Component {
  render() {
    return (
      <table className="table table-striped table-sm table-bordered">
        <thead>
          <tr className="bg-primary text-white">
            <th scope="col">Актив</th>
            <th scope="col">Текущая стоимость</th>
            <th scope="col">Прибыль</th>
            <th scope="col">P/L за день</th>
            <th scope="col">P/L за день, %</th>
            <th scope="col">Текущая доля</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td colspan="2">Larry the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

export default TableActive;
