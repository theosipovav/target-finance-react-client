import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle, faTrash, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const PortfolioListRow = (props) => {
  return (
    <tr>
      <th scope="row">{props.index}</th>
      <td>{props.name}</td>
      <td>{props.portfolioName}</td>
      <td>{props.commissionFixed}</td>
      <td>{props.dtOpening}</td>
      <td>{props.note}</td>
      <td>
        <div className="btn-group d-flex justify-content-center align-items-center" role="group" aria-label="Basic outlined example">
          <Link to={"" + props.portfolioId} className="btn btn-outline-primary">
            <FontAwesomeIcon icon={faFolderOpen} />
          </Link>
          <button type="button" className="btn btn-outline-primary" onClick={props.clickOpen}>
            <FontAwesomeIcon icon={faFolderOpen} />
          </button>
          <button type="button" className="btn btn-outline-primary">
            <FontAwesomeIcon icon={faInfoCircle} />
          </button>
          <button type="button" className="btn btn-outline-primary">
            <FontAwesomeIcon icon={faTrash} />
          </button>
        </div>
      </td>
    </tr>
  );
};

export default PortfolioListRow;
