import React, { Component } from "react";
import styles from "./TradeTable.module.scss";
import BTable from "react-bootstrap/Table";
import { useTable, useSortBy, usePagination, useFilters, useGlobalFilter, useAsyncDebounce } from "react-table";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSortDown, faSortUp, faSort, faEye, faTrash, faEdit, faAlignJustify, faSearch } from "@fortawesome/free-solid-svg-icons";
import { Table, Button, Form, Pagination, Dropdown, FloatingLabel } from "react-bootstrap";
import { Link, Navigate } from "react-router-dom";
import { matchSorter } from "match-sorter";

class TradeTable extends Component {
  render() {
    var props = this.props;

    const columns = [
      {
        Header: "№",
        id: "idColumn",
        accessor: "tradeId",
        Cell: ({ row }) => {
          return <div className="d-flex justify-content-center">{row.id}</div>;
        },
      },
      { Header: "Портфель", accessor: "portfolio.name" },
      { Header: "Актив", accessor: "assetCategory.name" },
      { Header: "Тикер", accessor: "symbol.value" },
      { Header: "Валюта", accessor: "currency.symbol" },
      {
        Header: "Дата/Время",
        accessor: "dt",
        Cell: ({ row }) => {
          var dt = new Date(row.original.dt);
          var month = dt.getMonth() + 1;
          var day = dt.getDate();
          if (month < 10) month = "0" + month;
          if (day < 10) day = "0" + day;
          var dtString = day + "-" + month + "-" + dt.getFullYear();
          return dtString;
        },
      },
      { Header: "Кол-во", accessor: "count" },
      { Header: "Выручка", accessor: "proceed" },
      { Header: "Статус", accessor: "status.name" },
      {
        Header: () => null,
        id: "buttons",
        Cell: ({ row }) => {
          return (
            <div className="d-flex justify-content-center">
              <Dropdown className="flex-grow-1">
                <Dropdown.Toggle variant="link" id="dropdown-basic" className={styles.DropdownToggle}>
                  <FontAwesomeIcon icon={faAlignJustify} />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item variant="link" onClick={(e) => props.handleModalDetail(row.original.tradeId)}>
                    <FontAwesomeIcon icon={faEye} /> Просмотреть
                  </Dropdown.Item>
                  <Dropdown.Item variant="link" onClick={(e) => props.handleModalEdit(row.original.tradeId)} className="text-info">
                    <FontAwesomeIcon icon={faEdit} /> Редактировать
                  </Dropdown.Item>
                  <Dropdown.Item variant="link" onClick={(e) => props.handleRemove(row.original.tradeId)} className="text-danger">
                    <FontAwesomeIcon icon={faTrash} /> Удалить
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          );
        },
      },
    ];
    const data = props.trades;
    return <RenderTable class="table" columns={columns} data={data} />;
  }
}

/**
 * Рендер таблицы
 * @param {*} param
 * @returns
 */
function RenderTable({ columns, data }) {
  const defaultColumn = {
    // Let's set up our default Filter UI
    Filter: DefaultColumnFilter,
  };

  const filterTypes = {
    // Add a new fuzzyTextFilterFn filter type.
    fuzzyText: fuzzyTextFilterFn,
    // Or, override the default text filter to use
    // "startWith"
    text: (rows, id, filterValue) => {
      return rows.filter((row) => {
        const rowValue = row.values[id];
        return rowValue !== undefined ? String(rowValue).toLowerCase().startsWith(String(filterValue).toLowerCase()) : true;
      });
    },
  };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    visibleColumns,
    preGlobalFilteredRows,
    setGlobalFilter,
    state: { pageIndex, pageSize },
  } = useTable({ columns, data, defaultColumn, filterTypes, initialState: { pageIndex: 0 } }, useFilters, useGlobalFilter, useSortBy, usePagination);
  const firstPageRows = rows.slice(0, 10);
  return (
    <React.Fragment>
      <div className="d-flex">
        <div className="flex-grow-1">
          <RenderFilterSearch preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={state.globalFilter} setGlobalFilter={setGlobalFilter} />
        </div>
        <div className="flex-grow-0 ms-3">
          <Pagination>
            <Pagination.First onClick={() => gotoPage(0)} disabled={!canPreviousPage} />
            <Pagination.Prev onClick={() => previousPage()} disabled={!canPreviousPage} />
            <Pagination.Next onClick={() => nextPage()} disabled={!canNextPage} />
            <Pagination.Last onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage} />
          </Pagination>
        </div>
      </div>
      <BTable hover size="sm" className={styles.TableTrade} {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  <div className="d-flex">
                    <div className="flex-grow-1"> {column.render("Header")}</div>
                    <div className="mx-1">
                      {column.id == "buttons" ? null : column.isSorted ? (
                        column.isSortedDesc ? (
                          <FontAwesomeIcon icon={faSortDown} />
                        ) : (
                          <FontAwesomeIcon icon={faSortUp} />
                        )
                      ) : (
                        <FontAwesomeIcon icon={faSort} />
                      )}
                    </div>
                  </div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
                })}
              </tr>
            );
          })}
        </tbody>
      </BTable>
      <div className="d-flex align-items-center">
        <div className="flex-grow-1">
          <Pagination>
            <Pagination.First onClick={() => gotoPage(0)} disabled={!canPreviousPage} />
            <Pagination.Prev onClick={() => previousPage()} disabled={!canPreviousPage} />
            {pageIndex > 0 ? <Pagination.Item onClick={() => gotoPage(pageIndex)}>{pageIndex}</Pagination.Item> : null}
            <Pagination.Item active>{pageIndex + 1}</Pagination.Item>
            {pageIndex < pageCount - 1 ? <Pagination.Item onClick={() => gotoPage(pageIndex + 1)}>{pageIndex + 2}</Pagination.Item> : null}
            <Pagination.Next onClick={() => nextPage()} disabled={!canNextPage} />
            <Pagination.Last onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage} />
          </Pagination>
        </div>
        <div className="mx-3">
          <span>
            Страница{" "}
            <b>
              {pageIndex + 1} из {pageOptions.length}
            </b>
          </span>
        </div>
        <div className="d-flex">
          <Form.Select
            aria-label="Показать по"
            defaultValue={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            <option disabled>Показать по</option>
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Показать по {pageSize}
              </option>
            ))}
          </Form.Select>
        </div>
      </div>
    </React.Fragment>
  );
}

function RenderFilterSearch({ preGlobalFilteredRows, globalFilter, setGlobalFilter }) {
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  return (
    <Form.Group className="d-flex align-items-center">
      <Form.Label className="d-flex justify-content-center align-items-center m-0 mx-2">
        <FontAwesomeIcon icon={faSearch} />
      </Form.Label>
      <Form.Control
        type="text"
        placeholder="Поиск"
        onChange={(e) => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
      />
    </Form.Group>
  );
}

// Define a default UI for filtering
function DefaultColumnFilter({ column: { filterValue, preFilteredRows, setFilter } }) {
  const count = preFilteredRows.length;
  return (
    <input
      value={filterValue || ""}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
    />
  );
}

function fuzzyTextFilterFn(rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [(row) => row.values[id]] });
}

// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = (val) => !val;

export default TradeTable;
