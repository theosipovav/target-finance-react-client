

import React, { Component } from 'react'
import { Routes, Route } from "react-router-dom";
import Layout from "./hoc/layout/Layout"
import Header from "./hoc/Header/Header";
import Main from "./hoc/Main/Main";
import About from "./pages/About/About";
import Footer from "./hoc/Footer/Footer";
import Portfolio from "./pages/Portfolio/Portfolio";
import PortfolioDetail from "./pages/PortfolioDetail/PortfolioDetail"
import Home from './pages/Home/Home';
import Signup from './pages/Signup/Signup';
import Signin from './pages/Signin/Signin';
import Logout from './pages/Logout/Logout';
import uuid from 'react-uuid';

import { connect } from 'react-redux';
import { autoLogin } from './store/actions/ActionLogin';
import Import from './pages/Import/Import';
import Trades from './pages/Trades/Trades';
import PortfolioCreate from './pages/PortfolioCreate/PortfolioCreate';
import MessageError from './components/MessageError/MessageError';
import TradeCreate from './pages/TradeCreate/TradeCreate';
import TradeEdit from './pages/TradeEdit/TradeEdit';
import UserControl from './pages/UserControl/UserControl';
import PortfolioEdit from './pages/PortfolioEdit/PortfolioEdit';
import UserCard from './pages/UserCard/UserCard';
import Currency from './pages/Currency/Currency';
import AssetCategory from './pages/AssetCategory/AssetCategory';
import Status from './pages/Status/Status';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { createBrowserHistory } from 'history';
import { Button, ListGroup } from 'react-bootstrap';

export class App extends Component {


  state = {

  };




  constructor(props) {
    super(props);

  }

  render() {
    const routesAuth = [
      { path: "/", Component: Home },
      { path: "/user-card", Component: UserCard },
      { path: "/user-control", Component: UserControl },
      { path: "/currency", Component: Currency },
      { path: "/assetCategory", Component: AssetCategory },
      { path: "/status", Component: Status },
      { path: "/portfolio", Component: Portfolio },
      { path: "/portfolio/create", Component: PortfolioCreate },
      { path: "/portfolio/edit/:id", Component: PortfolioEdit },
      { path: "/portfolio/:id", Component: PortfolioDetail },
      { path: "/trades", Component: Trades },
      { path: "/trade", Component: Trades },
      { path: "/trade/edit/*", Component: TradeEdit },
      { path: "/trade/create", Component: TradeCreate },
      { path: "/import", Component: Import },
      { path: "/about", Component: About },
      { path: "/logout", Component: Logout }
    ];
    const routesNoAuth = [
      { path: "/", Component: Home },
      { path: "/signup", Component: Signup },
      { path: "/signin", Component: Signin },
      { path: "/about", Component: About }

    ]






    var history = createBrowserHistory();
    var location = history.location;
    var routes = this.props.isAuthenticated ? routesAuth : routesNoAuth;

    return (
      <Layout>
        <Header />
        <Main>
          <MessageError />
          <Routes>
            {routes.map(({ path, Component }) => {
              return <Route key={path} path={path} element={
                <React.Fragment>
                  <CSSTransition in={path == location.pathname ? true : false} timeout={300} classNames="page" >
                    <div className='page' >
                      < Component />
                    </div>
                  </CSSTransition>
                </React.Fragment>
              } />
            })}
          </Routes>
        </Main>
        <Footer />
      </Layout >
    );
  }


  componentDidMount() {
    this.props.actionAutoLogin();
  }
}


function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.user,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actionAutoLogin: () => dispatch(autoLogin()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)





