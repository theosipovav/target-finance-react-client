
import axios from "axios";

class ApiHelper {

    token = "";
    headers = {};

    constructor() {
        this.token = localStorage.getItem("token")
        const headers = {
            "Accept": "application/json",
            "Authorization": "Bearer " + this.token
        }
    }
    /**
     * Отправка POST запроса
     * @param {*} data 
     */
    async Post(url, data) {

        const response = await axios.post(url, data, { headers: this.headers });
        return response;
    }

    async Get(url) {
        const response = await axios.get(url, { headers: this.headers });
        return response;
    }


}

export default ApiHelper;