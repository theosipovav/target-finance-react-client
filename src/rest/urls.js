
const SERVER = "http://localhost:55789";

export const URL_GET_USER = SERVER + "/api/User/";
export const URL_LOGIN = SERVER + "/api/Account/Signin/";                // Войти в систему
export const URL_SIGNUP = SERVER + "/api/Account/Signup/";               // Регистрация
export const URL_IMPORT = SERVER + "/api/import/file/";
export const GET_ROLE_ALL = SERVER + "/api/Role";
export const GET_TRADES_PORTFOLIO = SERVER + "/api/Trade/GetTradesPortofolio/";
export const GET_PORTFOLIO_TYPE = SERVER + "/api/PortfolioType/";

export const GET_ASSET_CATEGORY_ALL = SERVER + "/api/AssetCategory/GetAll"              // Получить все записи о классе актива
export const POST_ASSET_CATEGORY_CREATE = SERVER + "/api/AssetCategory/Create";         // Создать новую запись о классе актива
export const PUT_ASSET_CATEGORY_UPDATE = SERVER + "/api/AssetCategory/Update";          // Обновить новую запись о классе актива
export const DELETE_ASSET_CATEGORY_DELETE = SERVER + "/api/AssetCategory/Remove";  // Удалить запись о классе актива по ее идентификатору

export const GET_CURRENCY_ALL = SERVER + "/api/Currency/GetAll";                        // Получить все записи о валюте
export const POST_CURRENCY_CREATE = SERVER + "/api/Currency/Create";                    // Создать новую запись о валюте
export const GET_CURRENCY_REMOVE = SERVER + "/api/Currency/Remove";                     // Удалить запись о валюте по ее идентификатору
export const GET_CURRENCY_UPDATE = SERVER + "/api/Currency/Update";                     // Обновить новую запись о валюте

export const GET_STATUS_ALL = SERVER + "/api/Status/GetAll";                            // Получить все записи о статусах заказа
export const POST_STATUS_CREATE = SERVER + "/api/Status/Create";                        // Создать новую запись о статусах заказа
export const PUT_STATUS_UPDATE = SERVER + "/api/Status/Update";                               // Обновить новую запись о статусах заказа
export const DELETE_STATUS_DELETE = SERVER + "/api/Status/Remove";                            // Удалить запись о статусах заказа по ее идентификатору

export const GET_SYMBOL_ALL = SERVER + "/api/Symbol/GetAll";

export const POST_CREATE_PORTFOLIO = SERVER + "/api/Portfolio/Create/";                 // Создание нового портфеля
export const GET_PORTFOLIOS_USER = SERVER + "/api/Portfolio/GetPortfoliosUser/";        // Получить все портфели текущего авторизованного пользователя
export const DELETE_PORTFOLIO_ID = SERVER + "/api/Portfolio/Delete";                    // Удалить портфель по его идентификатору
export const GET_PORTFOLIO_ID = SERVER + "/api/Portfolio/GetPortfolioById";             // Получить портфель по его идентификатору
export const POST_PORTFOLIO_UPDATE = SERVER + "/api/Portfolio/Update";                  // Обновить портфель

export const GET_SUMMARY_FROM_ASSET_CATEGORY = SERVER + "/api/Portfolio/GetSummaryFromAssetCategory"
export const GET_SUMMARY_FROM_ASSET_SYMBOLS = SERVER + "/api/Portfolio/GetSummaryFromSymbols"
export const GET_GROUP_BY_MONTH_FOR_PRICE = SERVER + "/api/Trade/GetGroupByMonthForPrice";

export const GET_TRADES = SERVER + "/api/Trade/GetTrades";
export const GET_TRADE_BY_ID = SERVER + "/api/Trade/GetOneById";
export const GET_TRADE_UPDATE = SERVER + "/api/Trade/Update";
export const POST_TRADE_CREATE = SERVER + "/api/Trade/Create";
export const REMOVE_TRADE_UPDATE = SERVER + "/api/Trade/Remove";

export const GET_LOGS = SERVER + "/api/Import/GetLogs";
export const DELETE_LOG = SERVER + "/api/Import/Remove";

export const GET_USERS = SERVER + "/api/Account/Users";
export const GET_USER_ALL = SERVER + "/api/User/All";
export const DELETE_USER = SERVER + "/api/User/Remove";
export const GET_USER_PASSWORD_RESET = SERVER + "/api/User/ResetPassword";
export const GET_USER_FOR_LOGIN = SERVER + "api/User/OneForLogin";