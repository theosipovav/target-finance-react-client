

class Helper {



    async GetAsync(url) {


        const headers = {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("token"),
        };
        return await axios.get(url, { headers: headers });
    }


}

export default Helper;